package common.communication;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

public class Hasher {
	
	/**
	 * Returns the md5 Hash of a given String as input.
	 * @param input
	 */
	public static String getMD5Hash(String input){
		try {
			MessageDigest hasher = MessageDigest.getInstance("MD5");
			hasher.update(input.getBytes("UTF-8")); //Fill hash method
			
			byte[] temp_key = hasher.digest(); //hash String
			
			BigInteger bigInt = new BigInteger(1,temp_key); // Get hash
			String hashtext = bigInt.toString(16); //convert the Integer to a string (in hex representation)
			
			hashtext = addLeadingZeros(hashtext);
			Logger.getLogger(Hasher.class).info("Hashed " + input + " to " + hashtext);
			
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String addLeadingZeros(String hashtext){
		// Add leading zeros
		while(hashtext.length() < 32 ){
			hashtext = "0"+hashtext;
		}
		
		return hashtext;
	}

}
