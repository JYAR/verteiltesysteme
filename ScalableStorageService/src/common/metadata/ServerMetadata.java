package common.metadata;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import common.communication.Communicator;

public class ServerMetadata implements ServerInterface {
	
	public static final String COMPONENT_DELIMITER = ",";
	
	protected String name;
	protected String ip;
	protected int port;
	private BigInteger endKey = null;
	private BigInteger startKey = null;
	private Communicator com;
	private Logger log = Logger.getRootLogger();
	
	public void setStartKey(String key){
		if (key != null) {
			startKey = new BigInteger(key, 16);
		} else {
			startKey = null;
		}
		
	}
	
	public BigInteger getStartKey(){
		return startKey;
	}
	
	public void setEndKey(String key){
		if (key != null) {
			endKey = new BigInteger(key, 16);	
		} else {
			endKey = null;
		}
	}
	
	public BigInteger getEndKey(){
		return endKey;
	}
	
	public String getEndKeyAsString(){
		String hashtext = endKey.toString(16); //convert the Integer to a string (in hex representation)
		
		// Add leading zeros
		while(hashtext.length() < 32 ){
		  hashtext = "0"+hashtext;
		}
		return hashtext;
	}
	
	public String getStartKeyAsString(){
		String hashtext = startKey.toString(16); //convert the Integer to a string (in hex representation)
		
		// Add leading zeros
		while(hashtext.length() < 32 ){
		  hashtext = "0"+hashtext;
		}
		return hashtext;
	}
	
	public boolean isResponsibleForKey(String key){
		BigInteger hashedKey = new BigInteger(key, 16);
		
		if (hashedKey.compareTo(endKey) <= 0 && hashedKey.compareTo(startKey) > 0) {
			return true;
		}
		
		return false;
	}
	
	public String getName() {
		return name;
	}
	
	public String getIp() {
		return ip;
	}
	
	public int getPort() {
		return port;
	}
	
	public void setName(String name) {
		this.name = name.trim();
	}
	
	public void setIp(String ip) {
		this.ip = ip.trim();
	}
	
	public void setPort(String port) {
		this.port = Integer.parseInt(port.trim());
	}
	
	public boolean hasCommunicator(){
		if (com != null) {
			return true;
		}
		return false;
	}
	
	/**
	 * Starts a new communicator if this was necessary
	 * @param in
	 * @param out
	 * @param socket
	 */
	public void startCommunicator(InputStream in, OutputStream out, Socket socket){
		this.com = new Communicator(in, out, socket);
	}
	
	public void startCommunicator(InputStream in, OutputStream out, Socket socket, int timeout){
		this.com = new Communicator(in, out, socket, timeout);
	}
	
	/**
	 * Returns the communicator which handles the connection with this server.
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public Communicator getCommunicator() throws UnknownHostException, IOException{
		if (!hasCommunicator()) { // Create new connection
			log.info("Creating new communicator for " + this.getIp() + ":" + this.getPort());
			Socket server = new Socket(this.getIp(), this.getPort());
			this.startCommunicator(server.getInputStream(), server.getOutputStream(), server);
		}
		return this.com;
	}
	
	public Communicator getCommunicator(int timeout) throws UnknownHostException, IOException{
		if (!hasCommunicator()) { // Create new connection
			log.info("Creating new communicator for " + this.getIp() + ":" + this.getPort());
			Socket server = new Socket(this.getIp(), this.getPort());
			this.startCommunicator(server.getInputStream(), server.getOutputStream(), server, timeout);
		}
		return this.com;
	}
	
	public void unsetCommunicator(){
		com = null;
	}
	
	/**
	 * Prepares a String to send over network connection.
	 */
	public String getAsString(){
		if (getStartKey() == null) {
			return "";
		}
		StringBuilder builder = new StringBuilder();
		builder.append(getName());
		builder.append(ServerMetadata.COMPONENT_DELIMITER);
		builder.append(getIp());
		builder.append(ServerMetadata.COMPONENT_DELIMITER);
		builder.append(getPort());
		builder.append(ServerMetadata.COMPONENT_DELIMITER);
		builder.append(getStartKeyAsString());
		builder.append(ServerMetadata.COMPONENT_DELIMITER);
		builder.append(getEndKeyAsString());
		return builder.toString();
	}
	
}
