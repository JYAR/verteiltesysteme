package common.metadata;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import common.communication.Communicator;
import common.communication.Hasher;

public class MetadataHandler {
	// Hole den kleinsten hash, wenn keiner verantwortlich
	
	protected List<ServerInterface> servers;
	private Logger log = Logger.getLogger(MetadataHandler.class);
	
	private static final String DELIMITER = ";";
	private static final int NUMBER_OF_ITEMS_FOR_SERVER = 5;
	
	/**
	 * Instantiates the metadatahandler
	 */
	public MetadataHandler(){
		servers = new LinkedList<ServerInterface>();
	}
	
	/**
	 * Returns communicator for the responsible server.
	 * @param key
	 * @return
	 */
	public synchronized Communicator getResponsibleServer(String key) throws UnknownHostException, IOException{
		ServerMetadata min = null;
		for (ServerInterface server: servers) {
			
			if (server.isResponsibleForKey(key)) {
				return server.getCommunicator();
			}
			
			//Search minimum if no responsible server was found.
			if (min == null){
				min = (ServerMetadata) server;
			} else {
				if (min.getEndKey().compareTo(server.getEndKey()) > 0) {
					min = (ServerMetadata) server;
				}
			}
			
		}
		
		return min.getCommunicator();
	}
	
	public synchronized String getResponsibleServerAsString(String key) {
		ServerMetadata min = null;
		for (ServerInterface server: servers) {
			
			if (server.isResponsibleForKey(key)) {
				return Hasher.addLeadingZeros(server.getEndKey().toString(16));
			}
			
			//Search minimum if no responsible server was found.
			if (min == null){
				min = (ServerMetadata) server;
			} else {
				if (min.getEndKey().compareTo(server.getEndKey()) > 0) {
					min = (ServerMetadata) server;
				}
			}
			
		}
		
		return Hasher.addLeadingZeros(min.getEndKey().toString(16));
	}
	
	/**
	 * Parses the String and creates the MetadataHandler structure.
	 * @param value
	 */
	public synchronized void parse(String value){
		log.info("Parse new metadata: " + value);
		
		servers = new LinkedList<ServerInterface>();
		
		log.info("Parsing metadata");
		String[] serverArray = value.split(DELIMITER);
		
		for (int i = 0; i < serverArray.length; i++) {
			log.info(serverArray[i]);
		}
		
		for (String serverEntry : serverArray) {
			if (!serverEntry.trim().equals("")) {
				ServerMetadata server = new ServerMetadata();
				
				String[] serverValues = serverEntry.split(ServerMetadata.COMPONENT_DELIMITER);
				if (serverValues.length != NUMBER_OF_ITEMS_FOR_SERVER) {
					StringBuilder build = new StringBuilder();
					for (int i = 0; i < serverValues.length; i++) {
						build.append(serverValues[i]);
					}
					log.info("Wrong metadata entry: " + build.toString());
					continue;
				}
				
				server.setName(serverValues[0]);
				server.setIp(serverValues[1]);
				server.setPort(serverValues[2]);
				server.setStartKey(serverValues[3]);
				server.setEndKey(serverValues[4]);
				
				servers.add(server);
			}
		}
		
		log.info("Parsing done!");
	}
	
	/**
	 * Returns the structure of the metadata as a string.
	 */
	public synchronized String getAsString(){
		StringBuilder builder = new StringBuilder();
		for (ServerInterface server : servers) {
			String serverString = server.getAsString();
			
			if (serverString.trim().equals("")) {
				
			} else {
				builder.append(serverString);
				builder.append(MetadataHandler.DELIMITER);
			}
		}
		
		return builder.toString();
	}	
}
