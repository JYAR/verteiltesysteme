package common.metadata;

import java.io.IOException;
import java.math.BigInteger;
import java.net.UnknownHostException;

import common.communication.Communicator;

public interface ServerInterface {

	public void setStartKey(String key);
	
	public BigInteger getStartKey();
	
	public void setEndKey(String key);
	
	public BigInteger getEndKey();
	
	public boolean isResponsibleForKey(String key);
	
	public String getName();
	
	public String getIp();
	
	public int getPort();
	
	public void setName(String name);
	
	public void setIp(String ip);
	
	public void setPort(String port);
	
	public boolean hasCommunicator();
	
	/**
	 * Returns the communicator which handles the connection with this server.
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public Communicator getCommunicator() throws UnknownHostException, IOException;
	
	public String getAsString();
}
