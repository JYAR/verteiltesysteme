package common.messages;

import common.communication.Hasher;



public class KeyValueMessage implements KVMessage {

	private String key;
	private String value;
	private StatusType status;
	private boolean valueSet = false;
	
	public static final byte MESSAGE_DELIMITER = 13; // Carriage return
	public static final byte COMPONENT_DELIMITER = 32; // Space
	public static final int MAX_KEY_SIZE = 32; // Maximum length of a key
	public static final int MAX_VALUE_SIZE = 120000; // Maximum length of a value
	
	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public StatusType getStatus() {
		return status;
	}
	
	/**
	 * Checks whether a given status is valid
	 * @param status
	 * @return
	 */
	public boolean isValidStatus(StatusType status){
		for(StatusType s : StatusType.values()){
			if (s == status) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Checks whether the set Status is a valid status for the call by a client.
	 * @return
	 */
	public boolean hasValidStatus(){
		if (status == StatusType.GET || status == StatusType.PUT) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns a boolean indicating whether a value was set (needed for differentiation between Update and Delete)
	 * @return
	 */
	public boolean hasValue(){
		return valueSet;
	}
	
	/**
	 * Sets the message Status
	 * @param status
	 */
	public void setStatus(StatusType status){
		this.status = status;
	}
	
	/**
	 * Sets the key of the message
	 * @param key
	 */
	public void setKey(String key){ //TODO: Make this method private, so that only the setKeyMD5Hash() method will be used.
		this.key = key;
	}
	
	public void setKeyMD5Hash(String clearKey){
		String hashedKey = Hasher.getMD5Hash(clearKey);
		setKey(hashedKey);
	}
	
	/**
	 * Sets the value of the message
	 * @param value
	 */
	public void setValue(String value){
		if (value != null && value.trim() != "") {
			this.valueSet = true;
			this.value = value;
		}
	}
	
	/**
	 * This method builds a KVMessage based on the current status of the variables.
	 * @return Byte-Array representing message to be send to client
	 */
	public byte[] toByteArray(){
		//Build normal String and afterwards build a byte array from that
		
		String message = "";
		message +=  status;
		
		// If there was a key print it.
		if(key != null){
			message += (char) COMPONENT_DELIMITER;
			message += key;
		}
		
		//If there was only a value print only the value.
		if(value != null){
			message +=  (char) COMPONENT_DELIMITER + value;
		}
		
		System.out.println("Sending message:" + message);
		byte[] temp = message.getBytes();
		byte[] msg = new byte[temp.length + 1];
		for (int i = 0; i < temp.length; i++) {
			msg[i] = temp[i];
		}
		msg[temp.length] = MESSAGE_DELIMITER; //carriage return
		
		return msg;
	}

}
