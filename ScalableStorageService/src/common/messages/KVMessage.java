package common.messages;

public interface KVMessage {
	
    public enum StatusType {
    	GET,             		/* Get - request */
    	GET_ERROR,       		/* requested tuple (i.e. value) not found */
    	GET_SUCCESS,     		/* requested tuple (i.e. value) found */
    	PUT,               		/* Put - request */
    	PUT_TRANSFER,			/* Put from a server to another server */
    	PUT_SUCCESS,     		/* Put - request successful, tuple inserted */
    	PUT_UPDATE,      		/* Put - request successful, i.e., value updated */
    	PUT_ERROR,       		/* Put - request not successful */
    	DELETE_SUCCESS,  		/* Delete - request successful */
    	DELETE_ERROR,     		/* Delete - request successful */
    	SERVER_STOPPED,         /* Server is stopped, no requests are processed */
    	SERVER_WRITE_LOCK,      /* Server locked for out, only get possible */
    	SERVER_NOT_RESPONSIBLE,  /* Request not successful, server not responsible for key */
    	
    	META_DATA_TRANSFER, /* Indicates that (new) metadata is transfered to this server */
    	TRANSFER_ALL_DATA, /* Indicates that the server should transfer all its data to another server identified by its hash. */
    	
    	ECS_START, /* ECS call to the server, that the server should start serving clients. */
    	ECS_STOP, /* ECS call to the server, that this server should stop serving clients. */
    	ECS_SHUTDOWN, /* ECS call to the server, that this server should stop serving clients and shut down. */
    	
    	ACKNOWLEDGE_ECS /* Command is sent from server to ecs, when it has executed the command. */
}

	/**
	 * @return the key that is associated with this message, 
	 * 		null if not key is associated.
	 */
	public String getKey();
	
	/**
	 * @return the value that is associated with this message, 
	 * 		null if not value is associated.
	 */
	public String getValue();
	
	/**
	 * @return a status string that is used to identify request types, 
	 * response types and error types associated to the message.
	 */
	public StatusType getStatus();
	
}


