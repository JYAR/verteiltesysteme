package common.exceptions;

/**
 * Implements Exception that is thrown in case the client or server send a message that is too long to work with. Only sed internally.
 * @author Alexander Asselborn, Alexander Chrusciel, Yannick Rödl
 *
 */
public class CommandTooLongException extends Exception {

	private static final long serialVersionUID = 650199012780243911L;

}
