package app_kvServer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import common.communication.Communicator;
import common.messages.KVMessage.StatusType;
import common.messages.KeyValueMessage;
import common.metadata.MetadataHandler;

/**
 * Class that handles one client on the server.
 * 
 * @author Alexander Asselborn, Alexander Chrusciel, Yannick Rödl
 * 
 */
public class ClientConnection implements Runnable {

	private static Logger log = Logger.getRootLogger();
	private Socket client;
	private InputStream in;
	private OutputStream out;
	private boolean quit = false;
	private KVStorageServer kvstorageserver;
	private Communicator com;
	private MetadataHandler mdH;
	private KVServer kvs;

	/**
	 * Initiates the Class. Gets all Streams, prepares logging etc.
	 * 
	 * @param client
	 */
	public ClientConnection(Socket client, KVServer server, MetadataHandler meta) {
		this.client = client;
		this.kvs = server;
		this.mdH = meta;

		kvstorageserver = KVStorageServer.getKVStorageServer();
		try {
			in = client.getInputStream();
			out = client.getOutputStream();
		} catch (IOException e) {
			try {
				client.close();
				in.close();
				out.close();
			} catch (IOException e1) {
				log.debug("Could not close connection with client.", e);
			}
			log.debug("Problem with IO with client.", e);
		}
		com = new Communicator(in, out, client, 0);
		log.info("Client connected: " + client.getInetAddress());
	}

	@Override
	public void run() {
		while (!quit) {
			// Listen for input of client
			KeyValueMessage incoming = null;
			try {
				incoming = com.receiveMessage();
				if (incoming == null) {
					onDisconnect();
					return;
				}
			} catch (IOException e) {
				onDisconnect();
				log.debug("Error reading input from client"
						+ client.getInetAddress().getHostAddress()
						+ ". Disconnecting...", e);
				continue;
			}

			KeyValueMessage response = new KeyValueMessage();

			// identify command of client;
			switch (incoming.getStatus()) {
			case GET:

				if (kvs.isStarted() == false) {
					response.setStatus(StatusType.SERVER_STOPPED);
					break;
				} else {

					if (mdH.getResponsibleServerAsString(incoming.getKey()).equals(
							kvs.getServerkey())) {
						try {

							String value = kvstorageserver.get(incoming
									.getKey());
							response.setValue(value);
							response.setStatus(StatusType.GET_SUCCESS);
						} catch (KeyNotFoundException e) {
							response.setStatus(StatusType.GET_ERROR);
						}
					} else {
						response.setStatus(StatusType.SERVER_NOT_RESPONSIBLE);
						response.setKey(incoming.getKey());
						response.setValue(mdH.getAsString());
					}
					break;
				}
			case GET_ERROR:
				response.setStatus(StatusType.GET_ERROR);
				break;
			case PUT_ERROR:
				response.setStatus(StatusType.PUT_ERROR);
			case DELETE_ERROR:
			case DELETE_SUCCESS:
			case GET_SUCCESS:
			case PUT_SUCCESS:
			case PUT_UPDATE:
				break;
			case PUT:

				if (kvs.isStarted() == false) {
					response.setStatus(StatusType.SERVER_STOPPED);
					break;
				} // No break!!! This wanted.
			case PUT_TRANSFER:
					// check if we are responsible
					if (mdH.getResponsibleServerAsString(incoming.getKey()).equals(
							kvs.getServerkey())) {
						if (incoming.hasValue()
								&& !incoming.getValue().equals("null")) {

							if (kvstorageserver.hasKey(incoming.getKey())) {
								// Update
								kvstorageserver.put(incoming.getKey(),
										incoming.getValue());
								response.setStatus(StatusType.PUT_UPDATE);
								try {
									response.setValue(kvstorageserver
											.get(incoming.getKey()));
								} catch (KeyNotFoundException e) {
									// Cannot happen
									log.debug(
											"Newly added key not found. Key: "
													+ incoming.getKey(), e);
								}
							} else {
								// Insert
								kvstorageserver.put(incoming.getKey(),
										incoming.getValue());
								response.setStatus(StatusType.PUT_SUCCESS);
							}

						} else { // Delete

							try {
								kvstorageserver.put(incoming.getKey());
								response.setStatus(StatusType.DELETE_SUCCESS);
							} catch (KeyNotFoundException e) {
								response.setStatus(StatusType.DELETE_ERROR);
							}
						}
					} else {
						response.setStatus(StatusType.SERVER_NOT_RESPONSIBLE);
						response.setKey(incoming.getKey());
						response.setValue(mdH.getAsString());
					}
				break;

			case SERVER_STOPPED:
			case SERVER_WRITE_LOCK:
			case SERVER_NOT_RESPONSIBLE:

			case META_DATA_TRANSFER:
				kvs.setServerKey(incoming.getKey());
				mdH.parse(incoming.getValue());
				response.setStatus(StatusType.ACKNOWLEDGE_ECS);
				break;
			case TRANSFER_ALL_DATA:
				String serverKey = incoming.getValue();
				try {
					Communicator transferTo = mdH.getResponsibleServer(serverKey);
					transferDataTo(transferTo, serverKey);
				} catch (UnknownHostException e1) {
					log.error("Could not transfer metadata", e1);
				} catch (IOException e1) {
					log.error("IOError when transferring metadata", e1);
				}
				response.setStatus(StatusType.ACKNOWLEDGE_ECS);
				break;
			case ECS_START:
				if (kvs.isStarted() == false) {
					kvs.setStarted(true);
				}
				response.setStatus(StatusType.ACKNOWLEDGE_ECS);
				break;
			case ECS_STOP:
				if (kvs.isStarted() == true) {
					kvs.setStarted(false);
				}
				response.setStatus(StatusType.ACKNOWLEDGE_ECS);
				break;
			case ECS_SHUTDOWN:
				// Send response
				try {
					kvs.closeSocket();
					response.setStatus(StatusType.ACKNOWLEDGE_ECS);
					com.sendMessage(response);
				} catch (IOException e) {
					onDisconnect();
					log.debug("Could not send response to client "
							+ client.getInetAddress().getHostName()
							+ "Disconnecting...", e);
				}
				kvs.stopServer();
				break;

			default:
				response.setStatus(StatusType.GET_ERROR);
				log.debug("Client sent wrong command: "
						+ client.getInetAddress().getHostName() + ".");
				break;
			}

			// Send response
			try {
				if (response.getStatus() != null) {
					com.sendMessage(response);
				}
			} catch (IOException e) {
				onDisconnect();
				log.debug("Could not send response to client "
						+ client.getInetAddress().getHostName()
						+ "Disconnecting...", e);
			}
		}

	}

	public void transferDataTo(Communicator toTransfer, String key){
		Set<String> allKeys = kvstorageserver.getAllKeys();
		
		List<String> keysToDelete = new LinkedList<String>();
		
		Iterator<String> it = allKeys.iterator();
		while(it.hasNext()){
			String curKey = it.next();
			
			log.info(mdH.getAsString());
			log.info("Responsible Server: " + mdH.getResponsibleServerAsString(curKey));
			log.info("Key: " + key);
			if (mdH.getResponsibleServerAsString(curKey).equals(key)) {
				log.info("Transferring key" + curKey);
				KeyValueMessage msg = new KeyValueMessage();
				msg.setStatus(StatusType.PUT_TRANSFER);
				msg.setKey(curKey);
				try {
					msg.setValue(kvstorageserver.get(curKey));
					toTransfer.sendMessage(msg);
					keysToDelete.add(curKey);
					log.info(toTransfer.receiveMessage().getStatus());
				} catch (IOException e) {
					log.error("Error transferring data",e);
				} catch (KeyNotFoundException e) {
					// Unhandled should not happen
				}
			}
		}
		
		log.info("Deleting keys from collection");
		for (Iterator<String> iterator = keysToDelete.iterator(); iterator.hasNext();) {
			String string = iterator.next();
			try {
				kvstorageserver.put(string);
			} catch (KeyNotFoundException e) {
				// Unhandled
			}
			
		}
		
		log.info("Done transferring keys");
	}
	
	/**
	 * Executed in case that the client disconnects.
	 */
	public void onDisconnect() {
		quit = true;
		log.info("Client " + client.getInetAddress().getHostName()
				+ " disconnected");
	}

}
