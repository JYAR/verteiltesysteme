package app_kvServer;

/**
 * Thrown by KVStorageServer in case the key does not exist for the HashMap.
 * @author Alexander Asselborn, Alexander Chrusciel, Yannick Rödl
 *
 */
public class KeyNotFoundException extends Exception {

	private static final long serialVersionUID = -3016892388104792457L;

}
