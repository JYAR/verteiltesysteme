package app_kvEcs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class ECSCLI {

	private static final String PROMPT = "ECS> ";
	
	private Logger log;
	
	public ECSCLI(){
		log = Logger.getLogger(ECSCLI.class);
	}
	
	/**
	 * Reads a line from System.in and checks whether it is a valid command. Then returns the line.
	 * @return
	 */
	public String readLine(){
		String line = null;
		
		System.out.print(PROMPT);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			line = br.readLine();
		} catch (IOException e) {
			log.info("Could not read from System.in", e);
			return null;
		}
		
		line.trim();
		
		// In case we do not have a valid command, we call ourself and try again.
		if (!isValidCommand(line)) {
			printHelp();
			line = readLine();
		}
		
		return line;
	}
	
	/**
	 * Checks whether the provided String contains a valid command.
	 * @param command
	 * @return
	 */
	public boolean isValidCommand(String command){
		if(command.equalsIgnoreCase("init") || command.equalsIgnoreCase("start") || 
				command.equalsIgnoreCase("stop") || command.equalsIgnoreCase("shutdown") ||
				command.equalsIgnoreCase("add") || command.equalsIgnoreCase("remove")){
			return true;
		}
		return false;
	}
	
	/**
	 * Prints a help for the user, stating what the user is allowed to do.
	 */
	public void printHelp(){
		System.out.println("<<< External Configuration Service >>>");
		System.out.printf("%-30s %-20s\n", "<Command>", "<Description>");
//		Start initial servers - init
		System.out.printf("%-30s %-20s\n", "init", "Initializes system with a randomly chosen number of servers. Does not start servers.");
//		Start storage service (call start)
		System.out.printf("%-30s %-20s\n", "start", "Starts all remotely connected servers.");
//		Stop the storage service (call stop)
		System.out.printf("%-30s %-20s\n", "stop", "Stops all available servers.");
//		Shutdown
		System.out.printf("%-30s %-20s\n", "shutdown","Stops all servers and shuts the whole service (including this client) down.");
//		Add a node
		System.out.printf("%-30s %-20s\n", "add", "Adds a storage node to the system.");
//		Remove a node
		System.out.printf("%-30s %-20s\n", "remove", "Removes a server node from the system.");
	}
	
	public void writeMessage(String message){
		System.out.println(message);
	}
	
}
