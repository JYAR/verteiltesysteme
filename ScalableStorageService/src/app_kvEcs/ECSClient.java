package app_kvEcs;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import logger.LogSetup;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import common.metadata.ServerInterface;

public class ECSClient {

	private Logger log;
	private boolean quit;
	private ECSCLI commandLine;
	private ECSCommunicator communicator;
	private String pathToScript;
	private String pathOnRemoteHost;
	
	public ECSClient(File config){
//		Initialize logging
		String logfile = "logs/ecs/ecs.log";
		try {
			new LogSetup(logfile, Level.INFO, true);
		} catch (IOException e) {
			System.out.println("Unable to initialize logging. Continue without logging.");
			e.printStackTrace();
		}
		
		log = Logger.getLogger(ECSClient.class);
		
		LinkedList<ServerInterface> nodes = readConfigurationFile(config);

//		Start the command line interface.
		commandLine = new ECSCLI();
		
//		Start the Communication Module
		communicator = new ECSCommunicator(nodes, pathToScript, pathOnRemoteHost);
	}
	
	/**
	 * Only needed for unit testing
	 * @return
	 */
	public ECSCommunicator getCommunicator(){
		return communicator;
	}
	
	/**
	 * Method that runs infinitely, until the service is shut down.
	 */
	public void start(){
		quit = false;
		
		while (!quit) {
//			Invoke CLI to get Response and react to that response.
			String command = commandLine.readLine();
			
			if (command.equalsIgnoreCase("init")) {
				communicator.initialize();
			} else if (command.equalsIgnoreCase("start")){
				communicator.start();
			} else if (command.equalsIgnoreCase("stop")){
				communicator.stop();
			} else if (command.equalsIgnoreCase("shutdown")){
				communicator.shutdown();
				shutdown();
			} else if (command.equalsIgnoreCase("add")){
				communicator.addNode();
			} else if (command.equalsIgnoreCase("remove")){
				communicator.removeNode(); //Removes a node.
			}
		}
	}
	
	/**
	 * Reads the configuration file and stores the given data in ServerNodes
	 * @param config path to file.
	 * @return List of nodes.
	 */
	public LinkedList<ServerInterface> readConfigurationFile(File config){
		LinkedList<ServerInterface> nodes = new LinkedList<ServerInterface>();

		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(config);
			Node root = doc.getFirstChild();

			log.info("Parsing configuration file");

			NodeList nodeList = root.getChildNodes();
			int length = nodeList.getLength();
			for (int i = 1; i < length; i++) {
				Node node = nodeList.item(i);
				if (node.getNodeName().equals("Servers")) { // Servers node
					NodeList servers = node.getChildNodes();
					for (int j = 1; j < servers.getLength(); j++) {
						Node server = servers.item(j);
						if (server.getNodeName().equals("Server")) { // A single server
							NodeList serverContentList = server.getChildNodes();
							// Create an internally handled node
							ServerNode serverNode = new ServerNode();
							
							for (int k = 1; k < serverContentList.getLength(); k++) {
								Node serverValues = serverContentList.item(k);
								if (serverValues.hasChildNodes()) {
									if (serverValues.getNodeName().equals("Name")) {
										serverNode.setName(serverValues.getFirstChild().getNodeValue());
									} else if( serverValues.getNodeName().equals("IP")){
										serverNode.setIp(serverValues.getFirstChild().getNodeValue());
									} else if (serverValues.getNodeName().equals("Port")){
										serverNode.setPort(serverValues.getFirstChild().getNodeValue());
									} else if (serverValues.getNodeName().equals("DontKill")){
										String dontKill = serverValues.getFirstChild().getNodeValue();
										if ( dontKill.equalsIgnoreCase("false")){
											serverNode.setDontKill(false);
										} else {
											serverNode.setDontKill(true);
										}
										
									}
								}
							}
							nodes.add(serverNode); //Add node to ECS management
						}
					}
					
				} else if (node.getNodeName().equals("Variables")){
					NodeList variables = node.getChildNodes();
					for (int j = 1; j < variables.getLength(); j++) {
						Node variable = variables.item(j);
						if (variable.getNodeName().equals("PATH_TO_START_SCRIPT")) {
							pathToScript = variable.getFirstChild().getNodeValue();
						} else if (variable.getNodeName().equals("PATH_TO_SERVER_ON_REMOTE_HOST")){
							pathOnRemoteHost = variable.getFirstChild().getNodeValue();
						}
					}
				}
				
			}
		} catch (ParserConfigurationException e) {
			log.error("Could not parse file", e);
			System.exit(-1);
		} catch (SAXException e) {
			log.error("Could not parse file", e);
			System.exit(-1);
		} catch (IOException e) {
			log.error("Probably file not found.", e);
			System.exit(-1);
		}
		log.info("Done parsing config.");
		
		return nodes;
	}
	
	public void shutdown(){
		quit = true;
	}
	
	/**
	 * Starts the ECSClient
	 * @param args command line arguments provided on startup of the ECSClient.
	 */
	public static void main( String[] args ){
		if (args.length == 1) {
			File f = new File(args[0]);
			if (!f.isFile()) {
				System.out.println("The provided argument does not point to a file. Try again.");
				return;
			}
//			Start the ECSClient
			ECSClient ecs = new ECSClient(f);
			ecs.start();
			
		} else {
			System.out.println("No config file specified. Try again.");
			return;
		}
	}
	
}
