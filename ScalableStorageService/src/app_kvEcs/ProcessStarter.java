package app_kvEcs;

import java.io.IOException;

public class ProcessStarter {
	
	public static void startNewServer(String pathToScript, String pathToServerOnRemoteHost, String ip, int port){
		String script = pathToScript + "script.bat";
		
		String[] cmd = new String[4];
		cmd[0] = script;
		cmd[1] = ip;
		cmd[2] = "" + port;
		cmd[3] = pathToServerOnRemoteHost;

		Runtime run = 	Runtime.getRuntime();
		try {
			run.exec(cmd);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
