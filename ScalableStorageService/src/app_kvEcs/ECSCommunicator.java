package app_kvEcs;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import common.communication.Hasher;
import common.metadata.MetadataHandler;
import common.metadata.ServerInterface;

public class ECSCommunicator extends MetadataHandler{
	
	private boolean initialized;
	private String pathToScript;
	private String pathOnRemoteHost;
	
	private static final int CLIENT_STARTUP_TIME = 10000;
	
	public ECSCommunicator(LinkedList<ServerInterface> nodes, String pathToScript, String pathOnRemoteHost){
		this.servers = nodes;
		
		this.pathToScript = pathToScript;
		this.pathOnRemoteHost = pathOnRemoteHost;
		
		initialized = false;
	}
	
	public void initialize(){
		if (initialized) { // Do not allow reinitializing
			return;
		}
		
		int numberOfNodes = servers.size();
		
		int startNodes = (int) (Math.floor(Math.random() * (numberOfNodes -1)) + 1); // Random number of nodes to be started between 1 and n;
	
		Iterator<ServerInterface> it = servers.iterator();
		//Choose the first startNodes nodes from the list and start them.
		while(startNodes > 0){
			ServerNode node = (ServerNode) it.next();
			
			startProcessViaSSH(node);
			node.setActive(true);
			
			startNodes--;
		}
		
		Logger.getLogger(ECSCommunicator.class).info("Waiting 5 seconds to let processes startup!");
		
		try {
			Thread.sleep(CLIENT_STARTUP_TIME);
		} catch (InterruptedException e) {
			// Unhandled
		}
		
		recalculateKeyRange();
		sendUpdatedKeyRanges();
		start();
		
		initialized = true;
	}
	
	/**
	 * Especially used for unit testing. To test functionality of remote server.
	 * @param node
	 * @return
	 */
	public void startProcessViaSSH(ServerNode node){
		ProcessStarter.startNewServer(pathToScript, pathOnRemoteHost, node.getIp(), node.getPort());
		String ip = node.getIp();
		String []ipparts = ip.split("@");
		node.setIp(ipparts[ipparts.length-1]);
		
	}
	
	public String getPathOnRemoteHost(){
		return pathOnRemoteHost;
	}
	
	public String getPathToScript(){
		return pathToScript;
	}
	
	public ServerNode getFirstNode(){
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
			return node;
		}
		
		return null;
	}
	
	/**
	 * Starts all servers that are active but not yet started.
	 */
	public void start(){
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
			
			if (node.isActive() && !node.isStarted()){
				node.start();
			}
			
		}
	}
	
	/**
	 * Sends the updated key ranges to all known servers, so that they know for which keys they are responsible from now on.
	 */
	public void sendUpdatedKeyRanges(){
//		Logger.getLogger(ECSCommunicator.class).info(this.getAsString());
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
			
			if (node.isActive()){
				node.sendMetadataUpdate(this.getAsString());
			}
		}
	}
	
	/**
	 * Calculates for every node, for which part this node is responsible for.
	 */
	public void recalculateKeyRange(){
		String[] nodes = new String[servers.size()];
		int index = 0;
		
		// Write data of all active servers into temporary array
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
			
			String serverKey;
			if ( node.isActive()){
				serverKey = Hasher.getMD5Hash(node.getIp() + ":" +	node.getPort());
				node.setEndKey(serverKey);
				
				nodes[index++] = serverKey;
			} else {
				node.setStartKey(null);
				node.setEndKey(null);
			}
		}
		
		//Index has number of entries
		//Copy to array with right size
		String[] active = new String[index];
		for (int i = 0; i < active.length; i++) {
			active[i] = nodes[i];
		}
		
		// Sort
		Arrays.sort(active);
		
		// Iterate over servers and search server with given key
		it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
		
			if (node.isActive()) {
				index = Arrays.binarySearch(active, node.getEndKey().toString(16)); //Gets entry in array
				
				if (index > 0) {
					node.setStartKey(active[index - 1]); // Set the start key
				} else {
					node.setStartKey("00000000000000000000000000000000"); // 0
				}
				
			}
		}
				
		
	}
	
	/**
	 * Shuts down the whole service.
	 */
	public void shutdown(){
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
			
			if (node.isActive()) {
				node.shutdown();	
			}
		}
	}
	
	/**
	 * Adds a node of the set to the service.
	 * @return
	 */
	public boolean addNode(){
		Iterator<ServerInterface> it = servers.iterator();
		
		ServerNode node = null;
		
		boolean started = false;
		while(it.hasNext()){
			node = (ServerNode) it.next();
			if (!node.isActive() ) {
				startProcessViaSSH(node);
				node.setActive(true);
				started = true;
				break;
			}
		}
		
		if(!started){ // return false if no server can be added.
			return false;
		}
		
		try {
			Logger.getLogger(ECSCommunicator.class).info("Process will get startup time of " + CLIENT_STARTUP_TIME);
			Thread.sleep(CLIENT_STARTUP_TIME);
		} catch (InterruptedException e) {
			// Unhandled
		}
		
		if (node != null) {
			recalculateKeyRange();
			sendUpdatedKeyRanges();
			BigInteger key = calculateAncestor(node);
			
			// Find node
			Iterator<ServerInterface> iti = servers.iterator();
			while (iti.hasNext()){
				ServerNode toContactForTransfer = (ServerNode) iti.next();
					
				if (toContactForTransfer.getEndKey() != null && toContactForTransfer.getEndKey().equals(key)) {
					toContactForTransfer.transferAllDataTo(node.getEndKeyAsString());
				}
			}
			
			start();
			return true;
		}
		
		return false;
	}
	
	/**
	 * Removes a random node from the set
	 */
	public void removeNode(){
		int numberOfStartedNodes = 0;
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
			if (node.isActive() && node.isStarted()) {
				numberOfStartedNodes++;
			}
		}
		
		Logger.getLogger(ECSCommunicator.class).info("Number of started nodes: " + numberOfStartedNodes );
		
		if(numberOfStartedNodes == 1){ // Dont kill last server
			return;
		}
		
		int remove = (int) Math.floor(Math.random() * numberOfStartedNodes );
		
		int index = 0;
		it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
			
			if (index == 0){
				index ++;
				continue;
			}
			
			if (node.isActive() && remove == 0 && !node.getDontKill()) {
				node.stop();
				
				BigInteger anc = calculateAncestor(node);
				node.setActive(false);
				recalculateKeyRange();
				node.setActive(true);
				sendUpdatedKeyRanges();
				
				// Find node
				Iterator<ServerInterface> iti = servers.iterator();
				while (iti.hasNext()){
					ServerNode toContactForTransfer = (ServerNode) iti.next();
						
					if (toContactForTransfer.getEndKey() != null && toContactForTransfer.getEndKey().equals(anc)) {
						node.transferAllDataTo(toContactForTransfer.getEndKeyAsString());
					}
				}
					
				node.shutdown();
			}
			
			if (node.isActive()) {
				remove--;
			}
		}
	}
	
	/**
	 * Calculates the predecessor and ancestor to send them currently available keys.
	 * @param node
	 * @return
	 */
	public List<BigInteger> calculatePredecessorAndAncestor(ServerNode node){
		List<BigInteger> neighbours = new LinkedList<BigInteger>();
		
		BigInteger[] keys = new BigInteger[servers.size()];
		
		int i = 0;
		
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode n = (ServerNode) it.next();
			if(n.isActive()) {
				keys[i] = n.getEndKey();
					
				i++;
				
			}
		}
		
		// Copy to new structure, then sort.
		BigInteger[] keysNew = new BigInteger[i];
		for (int j = 0; j < keysNew.length; j++) {
			keysNew[j] = keys[j];
		}
		
		Arrays.sort(keysNew);
		
		//Find endKey of Node
		for (int j = 0; j < keysNew.length; j++) {
			if (node.getEndKey().compareTo(keysNew[j]) == 0) {
				//Found the right key
				if (j == 0) {
					neighbours.add(keysNew[keysNew.length - 1]);
				} else {
					neighbours.add(keysNew[j - 1]);
				}
				
				neighbours.add(keysNew[j]);
				
				if (j == keysNew.length -1) {
					neighbours.add(keysNew[0]);
				} else {
					neighbours.add(keysNew[j + 1]);
				}
				
				break;
			}
		}
		
		
		
		return neighbours;
	}
	
	/**
	 * Calculates the predecessor and ancestor to send them currently available keys.
	 * @param node
	 * @return
	 */
	public BigInteger calculateAncestor(ServerNode node){
		BigInteger[] keys = new BigInteger[servers.size()];
		
		int i = 0;
		
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode n = (ServerNode) it.next();
			if(n.isActive() && n.getEndKey() != null) {
				keys[i] = n.getEndKey();
					
				i++;
				
			}
		}
		
		// Copy to new structure, then sort.
		BigInteger[] keysNew = new BigInteger[i];
		for (int j = 0; j < keysNew.length; j++) {
			keysNew[j] = keys[j];
		}
		
		Arrays.sort(keysNew);
		
		//Find endKey of Node
		for (int j = 0; j < keysNew.length; j++) {
			if (node.getEndKey().compareTo(keysNew[j]) == 0) {
				//Found the right key
				if (j == keysNew.length -1) {
					return keysNew[0];
				} else {
					return keysNew[j + 1];
				}
			}
		}
		return null;
	}

	/**
	 * Stops the whole service. So that all servers stop serving clients.
	 */
	public void stop(){
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
			
			if(node.isStarted()){
				node.stop();
			}
		}
	}
	
	public int getAmountOfNodes() {
		int counter = 0;
		Iterator<ServerInterface> it = servers.iterator();
		while(it.hasNext()){
			ServerNode node = (ServerNode) it.next();
		
			if (node.isActive()) {
				counter++;
			}
		}
		return counter;
	}
}
