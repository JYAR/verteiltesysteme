package app_kvEcs;

import java.io.IOException;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import common.communication.Communicator;
import common.communication.Hasher;
import common.messages.KVMessage.StatusType;
import common.messages.KeyValueMessage;
import common.metadata.ServerMetadata;

public class ServerNode extends ServerMetadata {
	
	private boolean active;
	private boolean started;
	private boolean dontKill;
	
	public void sendMetadataUpdate(String metadata){
		sendMessage(StatusType.META_DATA_TRANSFER, metadata);
	}
	
	public void setDontKill(boolean kill){
		dontKill = kill;
	}
	
	public boolean getDontKill(){
		return dontKill;
	}
	
	/**
	 * Sends the start command to a server.
	 */
	public void start(){
		sendMessage(StatusType.ECS_START);
		
		this.started = true;
	}
	
	/**
	 * This method shall remotely invoke the server and stop it.
	 */
	public void stop(){
		this.started = false;
		this.active = true;
		
		sendMessage(StatusType.ECS_STOP);
	}
	
	/**
	 * Message that the stated server should transfer all its data to the stated server.
	 * @param nodeKey
	 */
	public void transferAllDataTo(String nodeKey){
		sendMessage(StatusType.TRANSFER_ALL_DATA, nodeKey);
	}
	
	/**
	 * Simply sends a message to a server with the given statusType.
	 * @param status
	 */
	public void sendMessage(StatusType status){
		sendMessage(status, null);
	}
	
	public void sendMessage(StatusType status, String value){
		Communicator com;
		try {
			com = getCommunicator(0);
			
			KeyValueMessage msg = new KeyValueMessage();
			msg.setStatus(status);
			msg.setKey(getVerificationString());
			
			if (value != null) {
				msg.setValue(value);
			}
			
			com.sendMessage(msg);
			
			com.receiveMessage();
		} catch (UnknownHostException e) {
			Logger.getLogger(ServerNode.class).error("Could not connect to server on " + this.getIp() + ":" + this.getPort() + ".");
		} catch (IOException e) {
			unsetCommunicator();
			Logger.getLogger(ServerNode.class).error("Error with IO when connecting to server " + this.getIp() + ":" + this.getPort(), e);
		}
	}
	
	public String getVerificationString(){
		return Hasher.getMD5Hash(this.getIp()+":"+ this.getPort());
	}
	
	/**
	 * Indicates whether the server was started at IP and port and therefore listens to client requests.
	 * @return
	 */
	public boolean isStarted(){
		return started;
	}
	
	/**
	 * Indicates whether the server is actively serving clients.
	 * @return
	 */
	public boolean isActive(){
		return active;
	}
	
	public void setActive(boolean state){
		this.active = state;
	}
	
	public void shutdown(){
		sendMessage(StatusType.ECS_SHUTDOWN);
		
		started = false;
		active = false;
		unsetCommunicator();
	}

}
