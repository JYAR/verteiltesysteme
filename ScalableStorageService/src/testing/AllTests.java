package testing;

import junit.framework.Test;
import junit.framework.TestSuite;


public class AllTests {

//	static {
//		try {
//			new LogSetup("logs/testing/test.log", Level.ERROR);
//			new KVServer(50000);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//	
	
	public static Test suite() {
		TestSuite clientSuite = new TestSuite("Scalable Storage Service Test-Suite");
		clientSuite.addTestSuite(ConnectionTest.class);
		clientSuite.addTestSuite(InteractionTest.class); 
		clientSuite.addTestSuite(AdditionalTest.class);
		clientSuite.addTestSuite(PerformanceTest.class);
		return clientSuite;
	}
	
}
