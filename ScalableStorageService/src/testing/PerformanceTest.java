package testing;

import java.io.File;
import java.util.LinkedList;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.junit.Test;

import app_kvEcs.ECSClient;
import app_kvEcs.ECSCommunicator;
import app_kvEcs.ServerNode;
import client.KVStore;

public class PerformanceTest extends TestCase {

	private KVStore kvClient;
	private ECSClient ecs;
	private ECSCommunicator com;
	private Stopwatch stopwatch;

	public void setUp() {

		ecs = new ECSClient(new File("config.xml"));
		
		com = ecs.getCommunicator();
		com.initialize(); // Startup of at least one node
		
		stopwatch = new Stopwatch();
	}

	public void tearDown() {
		kvClient.disconnect();
		
		com.shutdown();
		ecs.shutdown();
	}
	
	public void setNodeSize(int nodeSize) throws Exception {
		int currentNodeSize = com.getAmountOfNodes();
		while (currentNodeSize < nodeSize) {
			System.out.println("Current amount of nodes:" + com.getAmountOfNodes() + ". Start increasing.");
			com.addNode();
			currentNodeSize = com.getAmountOfNodes();
		}
		while (currentNodeSize > nodeSize) {
			System.out.println("Current amount of nodes:" + com.getAmountOfNodes() + ". Start decreasing.");
			com.removeNode();
			currentNodeSize = com.getAmountOfNodes();
		}
		if (nodeSize != com.getAmountOfNodes()) {
			throw new Exception("The expected amount of nodes (" + nodeSize +") is not equal the actual amount of nodes (+" + com.getAmountOfNodes() +")");
		}
	}

	@Test
	public void testlatency1Client1Server() {
		// Amount of clients may not exceed amount of prepared directories
		int amountOfClients = 1;
		
		// Begin: Set amount of nodes
		try {
			setNodeSize(1);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		// End: Set amount of nodes
		
		// Begin: Get first server node
		ServerNode first = com.getFirstNode();
		// End: Get first server node
		
		// Begin: List to manage all parallel tests
		LinkedList<ParallelPerformanceTest> allTests = new LinkedList<ParallelPerformanceTest>();
		// End: List to manage all parallel tests
		
		// Begin: create new tests
		for (int i=1; i<=amountOfClients; i++) {
			ParallelPerformanceTest test = new ParallelPerformanceTest(new KVStore(first.getIp(), first.getPort()), new File("Y:/enron_mail_20110402/maildir/allen-p-client"+i));			
			allTests.add(test);
		}
		// End: create new tests
		
		try {
			stopwatch.start();
			Logger.getRootLogger().info("latencyTest1Client1Server(): Starting stopwatch.");
			// Start: block of code, which is measured
			for (ParallelPerformanceTest client : allTests) {
				client.start();				
			}
			// End: block of code, which is measured
			while (true) {
				boolean finished = true;
				for (ParallelPerformanceTest client : allTests) {
					if(!client.getDone()) {
						finished = false;
					}
				}
				if(finished) {
					break;
				}
				Thread.sleep(1000);
			}
			stopwatch.stop();
			Logger.getRootLogger().info("Latency test " + amountOfClients + " Clients Time: " + stopwatch);
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(true);
	}
	
	
//	public void testlatency5Client1Server() {
//		// Amount of clients may not exceed amount of prepared directories
//		int amountOfClients = 5;
//		
//		// Begin: Set amount of nodes
//		try {
//			setNodeSize(1);
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//		// End: Set amount of nodes
//		
//		// Begin: Get first server node
//		ServerNode first = com.getFirstNode();
//		// End: Get first server node
//		
//		// Begin: List to manage all parallel tests
//		LinkedList<ParallelPerformanceTest> allTests = new LinkedList<ParallelPerformanceTest>();
//		// End: List to manage all parallel tests
//		
//		// Begin: create new tests
//		for (int i=1; i<=amountOfClients; i++) {
//			ParallelPerformanceTest test = new ParallelPerformanceTest(new KVStore(first.getIp(), first.getPort()), new File("Y:/enron_mail_20110402/maildir/allen-p-client"+i));			
//			allTests.add(test);
//		}
//		// End: create new tests
//		
//		try {
//			stopwatch.start();
//			Logger.getRootLogger().info("latencyTest1Client1Server(): Starting stopwatch.");
//			// Start: block of code, which is measured
//			for (ParallelPerformanceTest client : allTests) {
//				client.start();				
//			}
//			// End: block of code, which is measured
//			while (true) {
//				boolean finished = true;
//				for (ParallelPerformanceTest client : allTests) {
//					if(!client.getDone()) {
//						finished = false;
//					}
//				}
//				if(finished) {
//					break;
//				}
//				Thread.sleep(1000);
//			}
//			stopwatch.stop();
//			Logger.getRootLogger().info("Latency test " + amountOfClients + " Clients Time: " + stopwatch);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		assertTrue(true);
//	}
//	
//	public void testlatency10Client1Server() {
//		// Amount of clients may not exceed amount of prepared directories
//		int amountOfClients = 10;
//		
//		// Begin: Set amount of nodes
//		try {
//			setNodeSize(1);
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//		// End: Set amount of nodes
//		
//		// Begin: Get first server node
//		ServerNode first = com.getFirstNode();
//		// End: Get first server node
//		
//		// Begin: List to manage all parallel tests
//		LinkedList<ParallelPerformanceTest> allTests = new LinkedList<ParallelPerformanceTest>();
//		// End: List to manage all parallel tests
//		
//		// Begin: create new tests
//		for (int i=1; i<=amountOfClients; i++) {
//			ParallelPerformanceTest test = new ParallelPerformanceTest(new KVStore(first.getIp(), first.getPort()), new File("Y:/enron_mail_20110402/maildir/allen-p-client"+i));			
//			allTests.add(test);
//		}
//		// End: create new tests
//		
//		try {
//			stopwatch.start();
//			Logger.getRootLogger().info("latencyTest1Client1Server(): Starting stopwatch.");
//			// Start: block of code, which is measured
//			for (ParallelPerformanceTest client : allTests) {
//				client.start();				
//			}
//			// End: block of code, which is measured
//			while (true) {
//				boolean finished = true;
//				for (ParallelPerformanceTest client : allTests) {
//					if(!client.getDone()) {
//						finished = false;
//					}
//				}
//				if(finished) {
//					break;
//				}
//				Thread.sleep(1000);
//			}
//			stopwatch.stop();
//			Logger.getRootLogger().info("Latency test " + amountOfClients + " Clients Time: " + stopwatch);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		assertTrue(true);
//	}
//	
//	public void testlatency5Client5Server() {
//		// Amount of clients may not exceed amount of prepared directories
//		int amountOfClients = 5;
//		
//		// Begin: Set amount of nodes
//		try {
//			setNodeSize(5);
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//		// End: Set amount of nodes
//		
//		// Begin: Get first server node
//		ServerNode first = com.getFirstNode();
//		// End: Get first server node
//		
//		// Begin: List to manage all parallel tests
//		LinkedList<ParallelPerformanceTest> allTests = new LinkedList<ParallelPerformanceTest>();
//		// End: List to manage all parallel tests
//		
//		// Begin: create new tests
//		for (int i=1; i<=amountOfClients; i++) {
//			ParallelPerformanceTest test = new ParallelPerformanceTest(new KVStore(first.getIp(), first.getPort()), new File("Y:/enron_mail_20110402/maildir/allen-p-client"+i));			
//			allTests.add(test);
//		}
//		// End: create new tests
//		
//		try {
//			stopwatch.start();
//			Logger.getRootLogger().info("latencyTest1Client1Server(): Starting stopwatch.");
//			// Start: block of code, which is measured
//			for (ParallelPerformanceTest client : allTests) {
//				client.start();				
//			}
//			// End: block of code, which is measured
//			while (true) {
//				boolean finished = true;
//				for (ParallelPerformanceTest client : allTests) {
//					if(!client.getDone()) {
//						finished = false;
//					}
//				}
//				if(finished) {
//					break;
//				}
//				Thread.sleep(1000);
//			}
//			stopwatch.stop();
//			Logger.getRootLogger().info("Latency test " + amountOfClients + " Clients Time: " + stopwatch);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		assertTrue(true);
//	}
//
//	public void testlatency5Client10Server() {
//		// Amount of clients may not exceed amount of prepared directories
//		int amountOfClients = 5;
//		
//		// Begin: Set amount of nodes
//		try {
//			setNodeSize(10);
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//		// End: Set amount of nodes
//		
//		// Begin: Get first server node
//		ServerNode first = com.getFirstNode();
//		// End: Get first server node
//		
//		// Begin: List to manage all parallel tests
//		LinkedList<ParallelPerformanceTest> allTests = new LinkedList<ParallelPerformanceTest>();
//		// End: List to manage all parallel tests
//		
//		// Begin: create new tests
//		for (int i=1; i<=amountOfClients; i++) {
//			ParallelPerformanceTest test = new ParallelPerformanceTest(new KVStore(first.getIp(), first.getPort()), new File("Y:/enron_mail_20110402/maildir/allen-p-client"+i));			
//			allTests.add(test);
//		}
//		// End: create new tests
//		
//		try {
//			stopwatch.start();
//			Logger.getRootLogger().info("latencyTest1Client1Server(): Starting stopwatch.");
//			// Start: block of code, which is measured
//			for (ParallelPerformanceTest client : allTests) {
//				client.start();				
//			}
//			// End: block of code, which is measured
//			while (true) {
//				boolean finished = true;
//				for (ParallelPerformanceTest client : allTests) {
//					if(!client.getDone()) {
//						finished = false;
//					}
//				}
//				if(finished) {
//					break;
//				}
//				Thread.sleep(1000);
//			}
//			stopwatch.stop();
//			Logger.getRootLogger().info("Latency test " + amountOfClients + " Clients Time: " + stopwatch);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		assertTrue(true);
//	}
//	
//	public void testlatency10Client10Server() {
//		// Amount of clients may not exceed amount of prepared directories
//		int amountOfClients = 10;
//		
//		// Begin: Set amount of nodes
//		try {
//			setNodeSize(10);
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//		// End: Set amount of nodes
//		
//		// Begin: Get first server node
//		ServerNode first = com.getFirstNode();
//		// End: Get first server node
//		
//		// Begin: List to manage all parallel tests
//		LinkedList<ParallelPerformanceTest> allTests = new LinkedList<ParallelPerformanceTest>();
//		// End: List to manage all parallel tests
//		
//		// Begin: create new tests
//		for (int i=1; i<=amountOfClients; i++) {
//			ParallelPerformanceTest test = new ParallelPerformanceTest(new KVStore(first.getIp(), first.getPort()), new File("Y:/enron_mail_20110402/maildir/allen-p-client"+i));			
//			allTests.add(test);
//		}
//		// End: create new tests
//		
//		try {
//			stopwatch.start();
//			Logger.getRootLogger().info("latencyTest1Client1Server(): Starting stopwatch.");
//			// Start: block of code, which is measured
//			for (ParallelPerformanceTest client : allTests) {
//				client.start();				
//			}
//			// End: block of code, which is measured
//			while (true) {
//				boolean finished = true;
//				for (ParallelPerformanceTest client : allTests) {
//					if(!client.getDone()) {
//						finished = false;
//					}
//				}
//				if(finished) {
//					break;
//				}
//				Thread.sleep(1000);
//			}
//			stopwatch.stop();
//			Logger.getRootLogger().info("Latency test " + amountOfClients + " Clients Time: " + stopwatch);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		assertTrue(true);
//	}
//
//
//	
//	public void testlatency1Client5Server() {
//		// Amount of clients may not exceed amount of prepared directories
//		int amountOfClients = 1;
//		
//		// Begin: Set amount of nodes
//		try {
//			setNodeSize(5);
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//		// End: Set amount of nodes
//		
//		// Begin: Get first server node
//		ServerNode first = com.getFirstNode();
//		// End: Get first server node
//		
//		// Begin: List to manage all parallel tests
//		LinkedList<ParallelPerformanceTest> allTests = new LinkedList<ParallelPerformanceTest>();
//		// End: List to manage all parallel tests
//		
//		// Begin: create new tests
//		for (int i=1; i<=amountOfClients; i++) {
//			ParallelPerformanceTest test = new ParallelPerformanceTest(new KVStore(first.getIp(), first.getPort()), new File("Y:/enron_mail_20110402/maildir/allen-p-client"+i));			
//			allTests.add(test);
//		}
//		// End: create new tests
//		
//		try {
//			stopwatch.start();
//			Logger.getRootLogger().info("latencyTest1Client1Server(): Starting stopwatch.");
//			// Start: block of code, which is measured
//			for (ParallelPerformanceTest client : allTests) {
//				client.start();				
//			}
//			// End: block of code, which is measured
//			while (true) {
//				boolean finished = true;
//				for (ParallelPerformanceTest client : allTests) {
//					if(!client.getDone()) {
//						finished = false;
//					}
//				}
//				if(finished) {
//					break;
//				}
//				Thread.sleep(1000);
//			}
//			stopwatch.stop();
//			Logger.getRootLogger().info("Latency test " + amountOfClients + " Clients Time: " + stopwatch);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		assertTrue(true);
//	}
//
//	
//	public void testlatency1Client10Server() {
//		// Amount of clients may not exceed amount of prepared directories
//		int amountOfClients = 1;
//		
//		// Begin: Set amount of nodes
//		try {
//			setNodeSize(10);
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//		// End: Set amount of nodes
//		
//		// Begin: Get first server node
//		ServerNode first = com.getFirstNode();
//		// End: Get first server node
//		
//		// Begin: List to manage all parallel tests
//		LinkedList<ParallelPerformanceTest> allTests = new LinkedList<ParallelPerformanceTest>();
//		// End: List to manage all parallel tests
//		
//		// Begin: create new tests
//		for (int i=1; i<=amountOfClients; i++) {
//			ParallelPerformanceTest test = new ParallelPerformanceTest(new KVStore(first.getIp(), first.getPort()), new File("Y:/enron_mail_20110402/maildir/allen-p-client"+i));			
//			allTests.add(test);
//		}
//		// End: create new tests
//		
//		try {
//			stopwatch.start();
//			Logger.getRootLogger().info("latencyTest1Client1Server(): Starting stopwatch.");
//			// Start: block of code, which is measured
//			for (ParallelPerformanceTest client : allTests) {
//				client.start();				
//			}
//			// End: block of code, which is measured
//			while (true) {
//				boolean finished = true;
//				for (ParallelPerformanceTest client : allTests) {
//					if(!client.getDone()) {
//						finished = false;
//					}
//				}
//				if(finished) {
//					break;
//				}
//				Thread.sleep(1000);
//			}
//			stopwatch.stop();
//			Logger.getRootLogger().info("Latency test " + amountOfClients + " Clients Time: " + stopwatch);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		assertTrue(true);
//	}

}