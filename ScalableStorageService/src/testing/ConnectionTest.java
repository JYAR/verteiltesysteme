package testing;

import java.io.File;
import java.net.UnknownHostException;

import junit.framework.TestCase;
import app_kvEcs.ECSClient;
import app_kvEcs.ECSCommunicator;
import client.KVStore;


public class ConnectionTest extends TestCase {

	private static ECSClient ecs;
	private static ECSCommunicator com;
	
	public void setUp() {

		ecs = new ECSClient(new File("config.xml"));
		
		com = ecs.getCommunicator();
		com.initialize(); // Startup of at least one node	
	}

	public void tearDown() {		
		com.shutdown();
		ecs.shutdown();
		
		ecs = null;
		com = null;
		System.gc();
	}
	
	public void testConnectionSuccess() {
		
		Exception ex = null;
		
		KVStore kvClient = new KVStore("localhost", 50000);
		try {
			kvClient.connect();
		} catch (Exception e) {
			ex = e;
		}	
		
		assertNull(ex);
	}
	
	
	public void testUnknownHost() {
		Exception ex = null;
		KVStore kvClient = new KVStore("unknown", 50000);
		
		try {
			kvClient.connect();
		} catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex instanceof UnknownHostException);
	}
	
	
	public void testIllegalPort() {
		Exception ex = null;
		KVStore kvClient = new KVStore("localhost", 123456789);
		
		try {
			kvClient.connect();
		} catch (Exception e) {
			ex = e; 
		}
		
		assertTrue(ex instanceof IllegalArgumentException);
	}
	
	

	
}

