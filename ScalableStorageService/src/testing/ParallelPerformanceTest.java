package testing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import client.KVStore;

public class ParallelPerformanceTest extends Thread{
	
	private File file;
	private KVStore kvClient;
	private boolean done = false;
	
	public ParallelPerformanceTest (KVStore client, File directory) {
		this.kvClient = client;
		this.file = directory;
	}
	
	public void run() {
		try {
			kvClient.connect();
			StoreAllFilesIntoStorage(file);
			setDone(true);
		} catch (Exception e) {
			setDone(true);
			e.printStackTrace();
		}
		
	}
	
	private void setDone(boolean b) {
		this.done = b;
		
	}
	
	public boolean getDone() {
		return this.done;
	}

	private void StoreAllFilesIntoStorage(File file) throws Exception{
		if (file.isDirectory()) {
			  System.out.println("Searching directory ... " + file.getAbsoluteFile());
		 
		            //do you have permission to read this directory?	
			    if (file.canRead()) {
				for (File temp : file.listFiles()) {
				    if (temp.isDirectory()) {
				    	StoreAllFilesIntoStorage(temp);
				    }
				    else {
				    	kvClient.put(temp.getAbsolutePath(), getFileContent(temp));
				    }
			    	}
			    } else {
				System.out.println(file.getAbsoluteFile() + "Permission Denied");
			 }
		}	 
	}
	
	private String getFileContent(File file) throws Exception {
		StringBuilder content = new StringBuilder(); 
		if (file.isFile()) {
			if (file.canRead()){
				FileReader fr = new FileReader(file);
			    BufferedReader br = new BufferedReader(fr);
			    String row = "";

			    while( (row = br.readLine()) != null )
			    {
			    	content.append(row);
			    }

			    br.close();
			}
		}
		return content.toString().trim();
	}

}
