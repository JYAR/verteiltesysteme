package testing;

import java.io.File;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.junit.Test;

import app_kvEcs.ECSClient;
import app_kvEcs.ECSCommunicator;
import app_kvEcs.ProcessStarter;
import app_kvEcs.ServerNode;
import client.KVStore;

import common.communication.Hasher;
import common.messages.KVMessage;
import common.messages.KVMessage.StatusType;

public class AdditionalTest extends TestCase {

	private KVStore kvClient;
	private static ECSClient ecs;
	private static ECSCommunicator com;
	private static ServerNode first;

	
	public void setUp() {

		ecs = new ECSClient(new File("config.xml"));
		
		com = ecs.getCommunicator();
		com.initialize(); // Startup of at least one node
		
		first = com.getFirstNode();

		kvClient = new KVStore(first.getIp(), first.getPort());
		try {
			kvClient.connect();
		} catch (Exception e) {
		}
		
	}

	public void tearDown() {
		
		kvClient.disconnect();
		
		com.shutdown();
		ecs.shutdown();
		
		ecs = null;
		com = null;
		System.gc();
	}

	@Test
	public void testspezialCharkey() {

		String key = "a_§$%&/()=?";
		String value = "bar";
		KVMessage response = null;
		Exception ex = null;

		try {
			kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
		}

		try {

			response = kvClient.get(key);
		} catch (Exception e) {
			ex = e;
		}

		assertTrue(ex == null && response.getValue().equals("bar"));
	}

	@Test
	public void testspezialCharvalue() {

		String key = "thisNewKey";
		String value = "a!_$%&/()=?";
		KVMessage response = null;
		Exception ex = null;

		try {
			kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
		}

		try {

			response = kvClient.get(key);
		} catch (Exception e) {
			ex = e;
		}

		assertTrue(ex == null && response.getValue().equals("a!_$%&/()=?"));
	}

	public void testlonginputvalue() {

		String key = "otherKey";
		StringBuilder build = new StringBuilder();
		for (int i = 0; i < 160000; i++) {
			build.append("a");
		}

		String value = build.toString();
		KVMessage response = null;
		// Exception ex = null;

		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			// ex = e;
		}

		assertTrue(response.getStatus() == StatusType.PUT_ERROR);
	}

	public void testlonginputkey() {

		String key = "abcdefghijklmnopqrstuvwxyzhsdhfhuisdhiufhsuihfiusdhuifhiusdhfuihsuihfuisdhfu";
		String value = "bar";
		KVMessage response = null;
		// Exception ex = null;

		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			// ex = e;
		}

		assertTrue(response.getStatus() == StatusType.PUT_SUCCESS || response.getStatus() == StatusType.PUT_UPDATE);
	}

	public void testalreadyexistingConnection() {

		Exception ex = null;

		KVStore kvClient = new KVStore("localhost", 50000);
		try {
			kvClient.connect();
		} catch (Exception e) {
			ex = e;
		}

		try {
			kvClient.connect();
		} catch (Exception e) {
			ex = e;
		}

		assertNull(ex);
	}

	@Test
	public void testspaceinvalue() {

		String key = "SpaceInValue";
		String value = "abc dfg";
		KVMessage response = null;
		Exception ex = null;

		try {
			kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
		}

		try {

			response = kvClient.get(key);
		} catch (Exception e) {
			Logger.getLogger(AdditionalTest.class).error("ERROR",e);
			ex = e;
		}

		assertTrue(ex == null && response.getValue().equals("abc dfg"));
	}
	
// 
//	
//	New tests in ScalableStorageService
//	
//	
	@Test
	public void testDataTransferToOtherServer(){
		String key = "merry"; // is a1ce8e1c43bc97259557379589e051af
		String key2 = "christmas"; // is 3d4fe7a00bc6fb52a91685d038733d6f
		Exception ex = null;
		String value = "christmas";
		String value2 = "and a happy new year!";
		
		
		System.out.println("Host 1: " + Hasher.getMD5Hash("127.0.0.1:50000")); // is 358343938402ebb5110716c6e836f5a2
		System.out.println("Host 2: " + Hasher.getMD5Hash("127.0.0.1:50001")); // is dcee0277eb13b76434e8dcd31a387709
		System.out.println("Host 3: " + Hasher.getMD5Hash("127.0.0.1:50002")); // is b3638a32c297f43aa37e63bbd839fc7e
		// Therefore both keys should be at least assigned to host 3 but not host 1.
		// We now make sure that there are enough servers
		for (int i = 0; i < 5; i++) {
			com.addNode();
		}
		
		// Send data to server
		try {
			kvClient.put(key, value);
			kvClient.put(key2, value2);
		} catch (Exception e) {
			ex = e;
		}
		
		// Remove nodes, so that at most one node is present!
		
		for (int i = 0; i < 18; i++) {
			com.removeNode();
		}
		
		//Check validity
		assertTrue(ex == null);
		try {
			assertTrue(value.equals(kvClient.get(key).getValue()));
			assertTrue(value2.equals(kvClient.get(key2).getValue()));
		} catch (Exception e) {
			Logger.getLogger(AdditionalTest.class).error("Unable to get key",e);
			assertTrue(e == null);
		}
		
	}
	
	@Test
	public void testOneServerAlwaysAccessible(){
		for (int i = 0; i < 15; i++) { // We do not have more than 15 servers in our test configuration
			com.removeNode();
		}
		
		String key = "This Is a test";
		String value = "abc dfg";
		Exception ex = null;
		Exception ex2 = null;
		
		try {
			kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
		}
		
		String getValue = null;
		
		try {
			getValue = kvClient.get(key).getValue();
		} catch (Exception e) {
			ex2 = e;
		}
		
		assertTrue(getValue.equals(value) && ex2 == null && ex == null);
		
	}
	
	@Test
	public void testServerDoesNotServeClientsBeforeStartCommand(){
		Exception ex = null;
		KVMessage response = null;
		String key = "x";
		String value = "y";
		
		com.shutdown();
		
		ServerNode node = com.getFirstNode();
		String p1 = com.getPathOnRemoteHost();
		String p2 = com.getPathToScript();
		
		ProcessStarter.startNewServer(p2, p1, node.getIp(), node.getPort());
		try {
			Thread.sleep(5000); // Wait 5 seconds till startup is finished.
		} catch (InterruptedException e1) {
			// Unhandled
		} 
		
		KVStore client = new KVStore(node.getIp(), node.getPort());
		
		try {
			client.connect();
		} catch (Exception e) {
			ex =e;
		}
		
		assertTrue(ex == null);
		
		try {
			response = client.put(key, value);
		} catch (Exception e) {
			ex = e;
		}
		
		assertTrue(ex == null);
		
		assertTrue(!response.getStatus().equals(StatusType.PUT_SUCCESS));
	}
	
	@Test
	public void testClientResendsRequestWhenWrongServerCalled(){
		String key = "merry"; // is a1ce8e1c43bc97259557379589e051af
		Exception ex = null;
		String value = "christmas";
		
		// First remove all nodes except one.
		for (int i = 0; i < 15; i++) {
			com.removeNode();
		}
		
		try {
			kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
		}
		
		assertTrue(ex == null);
		
		//Add lots of servers!
		for (int i = 0; i < 15; i++) {
			com.addNode();
		}
		
		// Not responsible should be received now. Then resend request.
		String v = null;
		try {
			v = kvClient.get(key).getValue();
		} catch (Exception e) {
			Logger.getLogger(AdditionalTest.class).error("ERROR", e);
			ex = e;
		}
		
		assertTrue(ex == null);
		assertTrue(v.equals(value));
	}
	
	@Test
	public void testAddLotsOfServers(){
		boolean added = true;
		Exception e = null;
		for (int i = 0; i < 50; i++) { // We do not have that many nodes in config file for testing.
			try{
				added = com.addNode();
			} catch (Exception ex){
				e = ex;
			}
		}
		
		assertTrue(e == null);
		assertTrue(added == false);
		
	}
	
}
