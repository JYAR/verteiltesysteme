package client;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

import org.apache.log4j.Logger;

import common.communication.Communicator;
import common.messages.KVMessage;
import common.messages.KVMessage.StatusType;
import common.messages.KeyValueMessage;
import common.metadata.MetadataHandler;

public class KVStore implements KVCommInterface {

	private String serverAddress;
	private int serverPort;
	private static final Logger log = Logger.getRootLogger();
	private MetadataHandler metadatahandler;
	private Communicator communicator;
	
	public static final String PROMPT = "KVClient> ";
	
	public static final String SERVER_ADRESS = "127.0.0.1";
	public static final int SERVER_PORT = 50000;
	
	/**
	 * Initialize KVStore with address and port of KVServer
	 * @param address the address of the KVServer
	 * @param port the port of the KVServer
	 */
	public KVStore(String address, int port) {
		serverAddress = address;
		serverPort = port;
	}
	
	public void setServerAddress (String serverAddress){
		this.serverAddress = serverAddress;
	}
	
	public void setServerPort (int serverPort){
		this.serverPort = serverPort;
	}
	
	@Override
	public void connect() throws Exception {
		try {
			if (communicator != null) {
				System.out.println(PROMPT +"You are already connected to a server. Please disconnect first.");
				return;
			}
			Socket server = new Socket(serverAddress, serverPort);
			InputStream in = server.getInputStream();
			OutputStream out = server.getOutputStream();
			communicator = new Communicator(in, out, server);
			if(communicator == null) throw new NullPointerException();
		} catch (Exception e) {
			if (communicator != null) {
				try{
					disconnect();
				} catch (Exception doNotDealWith){
					//Do nothing here
				}
			}
			System.out.println(PROMPT +"Problems getting the streams. Please try again.");
			log.error("Problem with IO establishing connection " + serverAddress + ":" + serverPort, e);
			throw e;
		}
		System.out.println(PROMPT + "Successfully established connection to server.");
		log.info("Successfully connected to server " + serverAddress + " on port: " + serverPort );
	}

	@Override
	public void disconnect() {
		communicator.closeConnection();
	}

	@Override
	public KVMessage put(String key, String value) throws Exception {	
		//Create the message to be sent to the server
		KeyValueMessage outgoing = new KeyValueMessage();
		outgoing.setStatus(StatusType.PUT);
		outgoing.setKeyMD5Hash(key);
		outgoing.setValue(value);
		
		if (metadatahandler != null)
			communicator = metadatahandler.getResponsibleServer(outgoing.getKey());
		
		KVMessage serverResponse = sendMessage(outgoing);
		StatusType statusType = serverResponse.getStatus();
		switch(statusType) {
			case SERVER_NOT_RESPONSIBLE:
				// If the server is not responsible he delivers his current version of all existing servers along with their responsible key ranges
				// The local meta data gets updated and the client tries again to send his put request.
				if (metadatahandler == null) {
					disconnect();
					metadatahandler = new MetadataHandler();
				}
				//getResponsibleServer() expects a hashed key
				metadatahandler.parse(serverResponse.getValue());
				// Call put with the original key, not the hashed key
				return put(key, value);
			case SERVER_STOPPED:
				log.info(PROMPT + "Server responded:" + statusType.toString() + " during put(key,value). Storage service is not running at the moment. Please try later.");
				break;
			case SERVER_WRITE_LOCK:
				log.info(PROMPT + "Server responded:" + statusType.toString() + " during put(key,value). Only get(key) is possible at the moment. Please try later.");
				break;
			default:
				break;
		}	
		return serverResponse;
	}

	@Override
	public KVMessage get(String key) throws Exception {	
		//Create the message to be sent to the server
		KeyValueMessage outgoing = new KeyValueMessage();
		outgoing.setStatus(StatusType.GET);
		outgoing.setKeyMD5Hash(key);
		
		if (metadatahandler != null)
			//getResponsibleServer() expects a hashed key
			communicator = metadatahandler.getResponsibleServer(outgoing.getKey());
		
		KVMessage serverResponse = sendMessage(outgoing);
		StatusType statusType = serverResponse.getStatus();
		switch(statusType) {
			case SERVER_NOT_RESPONSIBLE:
			// If the server is not responsible he delivers his current version of all existing servers along with their responsible key ranges
			// The local meta data gets updated and the client tries again to send his put request.
			if (metadatahandler == null) {
				disconnect();
				metadatahandler = new MetadataHandler();
			}
			metadatahandler.parse(serverResponse.getValue());
			// Call get with the original key, not the hashed key
			return get(key);
			case SERVER_STOPPED:
				log.info(PROMPT + "Server responded:" + statusType.toString() + " during get(key). Storage service is not running at the moment. Please try later.");
				Thread.sleep(2000);
				return get(key);
			default:
				break;
		}
		return serverResponse;
	}
	
	/**
	 * Method sends a KVMessage using this socket.
	 * @param msg the message that is to be sent.
	 * @throws IOException some I/O error regarding the output stream 
	 */
	private KeyValueMessage sendMessage(KeyValueMessage msg) throws IOException {
		//Send the outgoing message to the server
		try{
			communicator.sendMessage(msg);
			//Receive response from server
		
			KeyValueMessage response = communicator.receiveMessage();
			return response;
		} catch (SocketException s) {
			log.error("Error during sendMessag(): ", s);
			return null;
		} catch (Exception e){
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			Socket s = new Socket(SERVER_ADRESS, SERVER_PORT); // Retry with always existing server
			communicator = new Communicator(s.getInputStream(), s.getOutputStream(), s);
			return sendMessage(msg);
		}
    }
}
