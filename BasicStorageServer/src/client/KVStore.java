package client;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import common.communication.Communicator;
import common.messages.KVMessage;
import common.messages.KVMessage.StatusType;
import common.messages.KeyValueMessage;

public class KVStore implements KVCommInterface {

	private String serverAddress;
	private int serverPort;
	private InputStream in;
	private OutputStream out;
	private Socket server;
	public static final String PROMPT = "KVClient> ";
	private static final Logger log = Logger.getRootLogger();
	
	/**
	 * Initialize KVStore with address and port of KVServer
	 * @param address the address of the KVServer
	 * @param port the port of the KVServer
	 */
	public KVStore(String address, int port) {
		this.serverAddress = address;
		this.serverPort = port;
	}
	
	@Override
	public void connect() throws Exception {
		try {
			if (server != null) {
				System.out.println(PROMPT +"You are already connected to a server. Please disconnect first.");
				return;
			}
			server = new Socket(serverAddress, serverPort);
			in = server.getInputStream();
			out = server.getOutputStream();
		} catch (UnknownHostException e) {
			System.out.println(PROMPT +"Could not determine the host. Maybe you entered a wrong address?");
			log.debug("Could not connect to host.", e);
			throw e;
		} catch (ConnectException e) {
			System.out.println(PROMPT +"Connection revoked from the Server. Please check IP-Adress.");
			log.debug("Connection revoked by Server", e);
			throw e;
		} catch (IOException e) {
			if (server != null) {
				try{
					server.close();
					in.close();
					out.close();
				} catch (Exception doNotDealWith){
					//Do nothing here
				}
			}
			System.out.println(PROMPT +"Problems getting the streams. Please try again.");
			log.debug("Problem with IO establishing connection", e);
			throw e;
		}
		System.out.println(PROMPT + "Successfully established connection to server.");
		log.info("Successfully connected to server " + serverAddress + " on port: " + serverPort );
		
	}

	@Override
	public void disconnect() {
		try {
			if (in != null) {
				in.close();
				out.close();
				server.close();
				in = null;
				out = null;
				server = null;
				System.out.println(PROMPT +"Successfully disconnected from server.");
			}
		} catch (IOException e) {
			System.out.println(PROMPT +"Could not close connection from server.");
			log.debug(PROMPT +"Could not close connection from server", e);
		}
	}

	@Override
	public KVMessage put(String key, String value) throws Exception {	
		//Create the message to be sent to the server
		KeyValueMessage outgoing = new KeyValueMessage();
		outgoing.setStatus(StatusType.PUT);
		outgoing.setKey(key);
		outgoing.setValue(value);
		
		return sendMessage(outgoing);
	}

	@Override
	public KVMessage get(String key) throws Exception {	
		//Create the message to be sent to the server
		KeyValueMessage outgoing = new KeyValueMessage();
		outgoing.setStatus(StatusType.GET);
		outgoing.setKey(key);
		
		return sendMessage(outgoing);
	}
	
	/**
	 * Method sends a KVMessage using this socket.
	 * @param msg the message that is to be sent.
	 * @throws IOException some I/O error regarding the output stream 
	 */
	private KeyValueMessage sendMessage(KeyValueMessage msg) throws IOException {
		Communicator com = new Communicator(in, out, server);
		//Send the outgoing message to the server
		com.sendMessage(msg);
		//Receive response from server
		KeyValueMessage response = com.receiveMessage();
		return response;
    }
}
