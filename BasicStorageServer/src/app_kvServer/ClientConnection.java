package app_kvServer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

import common.communication.Communicator;
import common.messages.KVMessage.StatusType;
import common.messages.KeyValueMessage;

/**
 * Class that handles one client on the server. 
 * @author Alexander Asselborn, Alexander Chrusciel, Yannick Rödl
 *
 */
public class ClientConnection implements Runnable {

	private static Logger log = Logger.getRootLogger();
	private Socket client;
	private InputStream in;
	private OutputStream out;
	private boolean quit = false;
	private KVStorageServer kvstorageserver;
	private Communicator com;
	
	/**
	 * Initiates the Class. Gets all Streams, prepares logging etc.
	 * @param client
	 */
	public ClientConnection(Socket client){
		this.client = client;
		kvstorageserver = KVStorageServer.getKVStorageServer();
		try {
			in = client.getInputStream();
			out = client.getOutputStream();
		} catch (IOException e) {
			try {
				client.close();
				in.close();
				out.close();
			} catch (IOException e1) {
				log.debug("Could not close connection with client.", e);
			}
			log.debug("Problem with IO with client.", e);
		}
		com = new Communicator(in, out, client);
		log.info("Client connected: " + client.getInetAddress());
	}
	
	@Override
	public void run() {
		while(!quit){
			// Listen for input of client
			KeyValueMessage incoming = null;
			try {
				incoming = com.receiveMessage();
				if (incoming == null) {
					onDisconnect();
					return;
				}
			} catch (IOException e) {
				onDisconnect();
				log.debug("Error reading input from client" + client.getInetAddress().getHostAddress() + ". Disconnecting...", e);
				continue;
			}
			
			KeyValueMessage response = new KeyValueMessage();
			
			//identify command of client;
			switch (incoming.getStatus()){
				case  GET: try{
								String value = kvstorageserver.get(incoming.getKey());
								response.setValue(value);
								response.setStatus(StatusType.GET_SUCCESS);
							} catch (KeyNotFoundException e){
								response.setStatus(StatusType.GET_ERROR);
							}
					break;
				case GET_ERROR:
					response.setStatus(StatusType.GET_ERROR);
					break;
				case PUT_ERROR:
					response.setStatus(StatusType.PUT_ERROR);
				case DELETE_ERROR:
				case DELETE_SUCCESS:	
				case GET_SUCCESS:
				case PUT_SUCCESS:
				case PUT_UPDATE: 
					break;
				case PUT:
					if (incoming.hasValue() && !incoming.getValue().equals("null")) {
						
						if (kvstorageserver.hasKey(incoming.getKey())) {
							// Update
							kvstorageserver.put(incoming.getKey(), incoming.getValue());
							response.setStatus(StatusType.PUT_UPDATE);
							try {
								response.setValue(kvstorageserver.get(incoming.getKey()));
							} catch (KeyNotFoundException e) {
								// Cannot happen
								log.debug("Newly added key not found. Key: " + incoming.getKey(),e);
							}
						} else {
							//Insert
							kvstorageserver.put(incoming.getKey(), incoming.getValue());
							response.setStatus(StatusType.PUT_SUCCESS);
						}
						
					} else { //Delete
						
						try{
							kvstorageserver.put(incoming.getKey());
							response.setStatus(StatusType.DELETE_SUCCESS);
						} catch ( KeyNotFoundException e){
							response.setStatus(StatusType.DELETE_ERROR);
						}
						
					}
					break;
				default:
					response.setStatus(StatusType.GET_ERROR);
					log.debug("Client sent wrong command: " + client.getInetAddress().getHostName() + ".");
					break;
			}
				
			
			// Send response
			try {
				com.sendMessage(response);
			} catch (IOException e) {
				onDisconnect();
				log.debug("Could not send response to client "+ client.getInetAddress().getHostName() + "Disconnecting...", e);
			}
		}
		

	}
	
	/**
	 * Executed in case that the client disconnects.
	 */
	public void onDisconnect(){
		quit = true;
		log.info("Client " + client.getInetAddress().getHostName() + " disconnected");
	}

}
