package app_kvServer;

import java.util.HashMap;
import java.util.Map;

/**
 * Storage Server with the HashMap. Implemented as singleton.
 * @author Alexander Asselborn, Alexander Chrusciel, Yannick Rödl
 *
 *
 */
public class KVStorageServer {
	private static KVStorageServer kvstorageserver;
	
	private Map<String, String> data;
	
	/**
	 * Private instance method to create singleton once on startup of server
	 */
	private KVStorageServer(){
		data = new HashMap<String, String>();
	}
	
	/**
	 * Retrieves a value for a given key from the key value store
	 * @param key
	 * @return
	 * @throws KeyNotFoundException
	 */
	public synchronized String get(String key) throws KeyNotFoundException{
		if(	data.containsKey(key)){
			return (String) data.get(key);
		} else {
			throw new KeyNotFoundException();
		}
	}
	
	/**
	 * Checks whether an entry with this key does already exist
	 * @param key
	 * @return
	 */
	public synchronized boolean hasKey( String key){
		if ( data.containsKey(key)){
			return true;
		}
		return false;
	}
	
	/**
	 * Delete from Key Value store
	 * @param key
	 * @throws KeyNotFoundException
	 */
	public synchronized void put(String key) throws KeyNotFoundException{
		if(data.containsKey(key)){
			data.remove(key);
		} else {
			throw new KeyNotFoundException();
		}
	}
	
	/**
	 * Insert into key value store
	 * @param key
	 * @param value
	 */
	public synchronized void put(String key, String value){
		data.put(key, value);
	}

	/**
	 * Method to get singleton of the key value store
	 * @return instance to KVStorageServer
	 */
	public static synchronized KVStorageServer getKVStorageServer(){
		if (kvstorageserver == null) {
			kvstorageserver = new KVStorageServer();
		}
		return kvstorageserver;
	}
}
