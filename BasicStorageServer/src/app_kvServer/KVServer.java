package app_kvServer;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;

import logger.LogSetup;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;


public class KVServer extends Thread {
	
	private static Logger logger = Logger.getRootLogger();
	
	private int port;
    private ServerSocket serverSocket;
    private boolean running;
	
	/**
	 * Start KV Server at given port
	 * @param port given port for storage server to operate
	 */
	public KVServer(int port) {
		this.port = port;
		this.start();
	}

    /**
     * Initializes and starts the server. 
     * Loops until the the server should be closed.
     */
    public void run() {
        
    	running = initializeServer();
        
        if(serverSocket != null) {
	        while(isRunning()){
	            try {
	                Socket client = serverSocket.accept();   
	                /**
	                 * Manages the client connection in a separate Thread in order to listen for requests from other threads.
	                 * This is necessary because we do not want to block the server when one client is connected.
	                 */
	                ClientConnection connection = new ClientConnection(client);
	                new Thread(connection).start();
	                
	                logger.info("Connected to " 
	                		+ client.getInetAddress().getHostName() 
	                		+  " on port " + client.getPort());
	            } catch (IOException e) {
	            	logger.error("Error! " +
	            			"Unable to establish connection. \n", e);
	            }
	        }
        }
        logger.info("Server stopped.");
    }
    
    /**
     * Checks whether the server is running (to stop externally via thread
     * @return true if server is allowed to continue running.
     */
    private boolean isRunning() {
        return this.running;
    }

    /**
     * Stops the server insofar that it won't listen at the given port any more.
     */
    public void stopServer(){
        running = false;
        try {
			serverSocket.close();
		} catch (IOException e) {
			logger.error("Error! " +
					"Unable to close socket on port: " + port, e);
		}
    }

    /**
     * Initializes the server and sets basic values.
     * @return idicates whether server could be started successfully
     */
    private boolean initializeServer() {
    	logger.info("Initialize server ...");
    	try {
            serverSocket = new ServerSocket(port);
            logger.info("Server listening on port: " 
            		+ serverSocket.getLocalPort());    
            return true;
        
        } catch (IOException e) {
        	logger.error("Error! Cannot open server socket:");
            if(e instanceof BindException){
            	logger.error("Port " + port + " is already bound!");
            }
            return false;
        }
    }
	
	/**
	 * Main method to start the server.
	 * @param args command line arguments (at the moment only port) to start server
	 */
	public static void main(String[] args){
		try {
			if(args.length < 1 || args.length > 2) {
				System.out.println("Error! Invalid number of arguments!");
				System.out.println("Usage: KVServer <port> <logLevel>(optional)!");
			} else {
				String logfile = "logs/server/server.log";
				if (args.length == 2) {
					Level log = Level.ALL;
					if (LogSetup.isValidLevel(args[1])) {
						log = Level.toLevel(args[1]);
					} else {
						System.out.println("Wrong log Level. Defaulting to all.");
					}
					new LogSetup(logfile, log);
				} else {
					new LogSetup(logfile, Level.ALL);
				}
				int port = Integer.parseInt(args[0]);
				new KVServer(port);
			}
		} catch (IOException e) {
			System.out.println("Error! Unable to initialize logger!");
			e.printStackTrace();
			System.exit(1);
		} catch (NumberFormatException nfe) {
			System.out.println("Error! Invalid argument <port>! Not a number!");
			System.out.println("Usage: Server <port>!");
			System.exit(1);
		}
    }
}
