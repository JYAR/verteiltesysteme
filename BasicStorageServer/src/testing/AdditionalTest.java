package testing;

import junit.framework.TestCase;

import org.junit.Test;

import client.KVStore;

import common.messages.KVMessage;
import common.messages.KVMessage.StatusType;

public class AdditionalTest extends TestCase {
	
	
	private KVStore kvClient;

	
	public void setUp() {
		kvClient = new KVStore("localhost", 50000);
		try {
			kvClient.connect();
		} catch (Exception e) {
		}
	}

	public void tearDown() {
		kvClient.disconnect();
	}
	
	@Test
	public void testspezialCharkey() {
		
		
			String key = "a_§$%&/()=?";
			String value = "bar";
			KVMessage response = null;
			Exception ex = null;

			try {
				kvClient.put(key, value);
			} catch (Exception e) {
				ex = e;
			}

			try {
				
				response = kvClient.get(key);
			} catch (Exception e) {
				ex = e;
			}
			
			assertTrue(ex == null && response.getValue().equals("bar"));
		}
	
	@Test
	public void testspezialCharvalue() {
		
		
			String key = "thisNewKey";
			String value = "a!_$%&/()=?";
			KVMessage response = null;
			Exception ex = null;

			try {
				kvClient.put(key, value);
			} catch (Exception e) {
				ex = e;
			}

			try {
				
				response = kvClient.get(key);
			} catch (Exception e) {
				ex = e;
			}
			
			assertTrue(ex == null && response.getValue().equals("a!_$%&/()=?"));
		}

	public void testlonginputvalue() {
		
		
		String key = "otherKey";
		StringBuilder build = new StringBuilder();
		for (int i = 0; i < 160000; i++) {
			build.append("a");
		}
		
		String value = build.toString();
		KVMessage response = null;
		Exception ex = null;

		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
		}
		
		assertTrue(response.getStatus() == StatusType.PUT_ERROR);
	}

public void testlonginputkey() {
		
		
		String key = "abcdefghijklmnopqrstuvwxyz";
		String value = "bar";
		KVMessage response = null;
		Exception ex = null;

		try {
			response = kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
		}
		
		assertTrue(response.getStatus() == StatusType.PUT_ERROR);
	}

public void testallreadyexsitingConnection() {
	
	Exception ex = null;
	
	KVStore kvClient = new KVStore("localhost", 50000);
	try {
		kvClient.connect();
	} catch (Exception e) {
		ex = e;
	}	
	
	try {
		kvClient.connect();
	} catch (Exception e) {
		ex = e;
	}	
	
	assertNull(ex);
}

@Test
public void testspaceinvalue() {
	
	
		String key = "SpaceInValue";
		String value = "abc dfg";
		KVMessage response = null;
		Exception ex = null;

		try {
			kvClient.put(key, value);
		} catch (Exception e) {
			ex = e;
		}

		try {
			
			response = kvClient.get(key);
		} catch (Exception e) {
			ex = e;
		}
		
		assertTrue(ex == null && response.getValue().equals("abc dfg"));
	}

}
