package app_kvClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

import logger.LogSetup;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import common.messages.KVMessage;
import common.messages.KeyValueMessage;

import client.KVStore;

/**
 * Handles the startup of the client and then gives the responsibility to KVStore.
 * @author Alexander Asselborn, Alexander Chrusciel, Yannick Rödl
 *
 */
public class KVClient {

	private static final Logger log = Logger.getRootLogger();
	private static KVStore client = null;
	
	/**
	 * Main method to start the client.
	 * @param args command line arguments to start the client
	 */
	public static void main(String[] args){
		try {
			new LogSetup("logs/client/client.log", Level.ALL);
		} catch (IOException e) {
			System.out.println("Error! Unable to initialize logger!");
			e.printStackTrace();
			System.exit(1);
		}
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		boolean quit = false;
		while ( !quit ) {
			System.out.print(KVStore.PROMPT);
			// Read user data
			String line = "";
			try {
				line = br.readLine();
			} catch (IOException e) {
				log.debug("Problems reading data from System.in", e);
			}
			
			//Split the arguments inserted by the user by one or more whitespace characters.
			String[] arguments = line.split("\\s+");
			
			if (arguments.length == 0) { // No input ready
				printHelp();
				continue;
			}
			
			// Connect to a server
			// Expected String: connect localhost 50000
			if(arguments[0].equals("connect")){
				if (arguments.length == 3) {
					log.debug("Trying to connect to server: " + arguments[1] + " on port: " + arguments[2]);
					try{
						client = new KVStore(arguments[1], Integer.parseInt(arguments[2]));
						client.connect();
					} catch (NumberFormatException nfe){
						System.out.println(KVStore.PROMPT + arguments[2] + " is not a number.");
						log.error("Argument is not a number: " + arguments[2], nfe);
						printHelp();
					} catch (UnknownHostException e) {
						System.out.println(KVStore.PROMPT + "Unknown Host!");
						log.error("Unknown Host!", e);
						
					} catch (Exception e) { // We are here in case we cannot connect to the server.
						client = null;
					}
				} else {
					System.out.println(KVStore.PROMPT +"Wrong call connect. See the help: ");
					printHelp();
				}
				
			} else if (arguments[0].equals("disconnect")) {
				
				if(connectionEstablished()){
					client.disconnect();
					client = null;
				}
				
			} else if (arguments[0].equals("put")){
				log.debug("Calling logic: put");
				if (!connectionEstablished()) {
					continue;
				}
				//Check amount of arguments
				//If amount == 2 -> delete
				//If amount == 3 -> put XOR update
				//Else: Error
				if (arguments.length == 2){
					log.debug("Logic:put; case:delete");
					log.debug("Key: " + arguments[1] + " Value: <null>");
					try {
						// Checks are OK - Invoke application logic.
						KVMessage response = client.put(arguments[1], null);
						printServerResponse(response);
						continue;
					}
					catch (Exception e) {
						log.error("Error during execution of logic put - delete", e);
						System.out.println("Error during execution of logic put. Please try again.");
						continue;
					}
				}
				else if (arguments.length >= 3) {
					log.debug("Logic:put; case: put or update");
					
					//Build value
					StringBuilder value = new StringBuilder();
					for (int i = 2; i < arguments.length; i++) {
						value.append(arguments[i]);
						if (i != arguments.length -1) {
							value.append((char) KeyValueMessage.COMPONENT_DELIMITER);
						}
					}
					
					log.debug("Key: " + arguments[1] + "Value: " + value.toString());
					try {
						// Checks are OK - Invoke application logic.
						KVMessage response = client.put(arguments[1], value.toString());
						printServerResponse(response);
						continue;
					}
					catch (Exception e) {
						log.error("Error during execution of logic put - put or update", e);
						System.out.println("Error during execution of logic put. Please try again.");
						continue;
					}
				}
				else {
					log.error("Wrong amount of parameters: " + arguments.length);
					System.out.println(KVStore.PROMPT +"Wrong amount of parameters (" + arguments.length +")." + "Use system as described below.");
					printHelp();
				}
			} else if (arguments[0].equals("get")){
				log.debug("Calling logic: get");
				if (!connectionEstablished()) {
					continue;
				}
				//Check amount of arguments
				//If amount == 2 -> Search for the element
				//Else: Error
				if (arguments.length == 2){
					log.debug("Logic:get; key == " + arguments[1]);
					try {
						// Checks are OK - Invoke application logic.
						KVMessage response = client.get(arguments[1]);
						printServerResponse(response);
						continue;
					}
					catch (Exception e) {
						log.error("Error during execution of logic get.", e);
						System.out.println("Error during execution of logic get. Please try again.");
						continue;
					}
				}
				else {
					log.error("Wrong amount of parameters: " + arguments.length);
					System.out.println(KVStore.PROMPT +"Wrong amount of parameters (" + arguments.length +")." + "Use system as described below.");
					printHelp();
				}
			} else if (arguments[0].equals("logLevel")){
				if ( arguments.length==2){
					if (LogSetup.isValidLevel((arguments[1]))) {
						log.setLevel(Level.toLevel(arguments[1]));
						System.out.println(KVStore.PROMPT +"Current Log Status: " + log.getLevel() );
					} else {
						System.out.println(KVStore.PROMPT +"Could not change log status.");
						System.out.println(KVStore.PROMPT +"Valid log status: " + LogSetup.getPossibleLogLevels());
					}
				}else {
					System.out.println(KVStore.PROMPT +"Please choose a logLevel");
					System.out.println(KVStore.PROMPT +"Valid log status: " + LogSetup.getPossibleLogLevels());
				}
			} else if (arguments[0].equals("help")) {
				printHelp();
			} else if (arguments[0].equals("quit")) {
				if (connectionEstablished()) {
					client.disconnect();
				}
				quit = true;
			
			} else {
				
				log.debug("Wrong command: " + arguments[0]);
				System.out.println(KVStore.PROMPT +"Could not identify the command. Use system as described below.");
				printHelp();
			
			}
		}
	}
	
	/**
	 * Prints a help for the user on how to use the echoClient.
	 */
	public static void printHelp(){
		System.out.println("<<< KVStore Client >>>");
		System.out.printf("%-30s %-20s\n", "<Command>", "<Description>");
		System.out.printf("%-30s %-20s\n", "connect %server% %port%", "Connect to a specified server on a specified port.");
		System.out.printf("%-30s %-20s\n", "disconnect", "Disconnect any available connection.");
		System.out.printf("%-30s %-20s\n", "help", "Displays this help output.");
		System.out.printf("%-30s %-20s\n", "logLevel %level%", "Choose of one of the following: " + LogSetup.getPossibleLogLevels());
		System.out.printf("%-30s %-20s\n", "quit", "Tears down the active connection to the server and exits the program execution.");
		System.out.printf("%-30s %-20s\n", "put %key% <%value%>", "Insert (New key, existing value), update (Existing key, new value) or delete (Existing key, but no %value% given) item on server.");
		System.out.printf("%-30s %-20s\n", "get %key%", "Receive item from server with identifier %key%");
	}
	
	/**
	 * Check if a connection is established to a server
	 * @return true, if connection is established, else false.
	 */
	private static boolean connectionEstablished() {
		if (client == null) {
			log.debug("client object is null.");
			log.error("No connection established.");
			printHelp();
		}
		return client != null;
	}
	
	/**
	 * Display the content of a KVMessage which was retrieved by the server.
	 * @param message
	 */
	private static void printServerResponse(KVMessage message) {
		if (message.getStatus() != null) {
			System.out.print(KVStore.PROMPT + message.getStatus());			
		}
		else{
			log.error("Server reponse does not contain any status.");
			System.out.println("Server reponse does not contain any status.");
		}
		
		if (message.getKey() != null) {
			System.out.print(" " + message.getKey());
		}
		if (message.getValue() != null) {
			System.out.print(" " + message.getValue());
		}
		System.out.println();
	}
}
