package common.communication;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import org.apache.log4j.Logger;

import common.exceptions.CommandTooLongException;
import common.messages.KVMessage.StatusType;
import common.messages.KeyValueMessage;

public class Communicator {
	
	private Logger log = Logger.getRootLogger();
	private InputStream in;
	private OutputStream out;
	private Socket socket;
	private boolean messageEndReached = false;
	
	public Communicator(InputStream in, OutputStream out, Socket socket){
		this.in = in;
		this.out = out;
		this.socket = socket;
	}

	/**
	 * Implements the agreed message format (see message.format.readme.txt). Builds a KeyValueMessage based on a Byte InputStream.
	 * @return Returns an instance of KeyValueMessage.
	 * @throws IOException
	 */
	public KeyValueMessage receiveMessage() throws IOException {
		String status = null;
				
		try {
			status = extractStatusFromStream();
		} catch (IOException e) {
			log.error("Failure in Communicator class, case: Status", e);
			e.printStackTrace();
			return null;
		} catch (CommandTooLongException e) {
			// Should not happen
			log.debug("Status too long", e);
			return null;
		}
		
		//Initialization of varX
		String var2 = null;
		String var4 = null;
		KeyValueMessage message = new KeyValueMessage();
		
		try{
			if (!message.isValidStatus(StatusType.valueOf(""+ status))) {
				return null;
			}
		} catch (IllegalArgumentException e){
			return null;
		}
		
		message.setStatus(StatusType.valueOf(""+status.toString()));
		StatusType statusType = message.getStatus();
		
		switch(statusType) {
		/*
		 * According to the protocol the following messages do not contain any key or any value information
		 */
		case GET_ERROR: break;
		case PUT_SUCCESS: break;
		case PUT_ERROR: break;
		case DELETE_SUCCESS: break;
		case DELETE_ERROR: break;
		
		case PUT_UPDATE: 
			try {
				var2 = extractValueFromStream();
			} catch (CommandTooLongException e) {
				// Should not happen!
				log.info("Command from server too long. Server corrupted", e);
			}
			message.setValue(var2);
			break;
			
		case PUT:
			messageEndReached = false;
			try {
				var2 = extractKeyFromStream();
				message.setKey(var2);
				if (!messageEndReached) {
					var4 = extractValueFromStream();
					message.setValue(var4.trim());
				}
			} catch (IOException e) {
				log.error("Failure in Communicator class, case:PUT", e);
				e.printStackTrace();
				return null;
			} catch (CommandTooLongException e) {
				message.setStatus(StatusType.PUT_ERROR);
			}
			var2 = null;
			var4 = null;
			break;
		
		case GET:
			try {
				var2 = extractKeyFromStream();
			} catch (IOException e) {
				log.error("Failure in Communicator class, case:GET", e);
				e.printStackTrace();
				return null;
			} catch (CommandTooLongException e) {
				message.setStatus(StatusType.GET_ERROR);
			}
			message.setKey(var2);
			var2 = null;
			break;
		
		case GET_SUCCESS:
			try {
				var2 = extractValueFromStream();
			} catch (IOException e) {
				log.error("Failure in Communicator class, case:GET_SUCCESS", e);
				e.printStackTrace();
				return null;
			} catch (CommandTooLongException e) {
				// Should not happen!
				log.info("Command from server too long. Server corrupted", e);
			}
			message.setValue(var2);
			var2 = null;
			break;
	}
		writeReceivedMessageLog(message);
		return message;
    }
	
	/**
	 * Only for logging purposes: A central method to display all relevant connection and message information
	 * @param message The message which is sent or retrieved.
	 */
	private void writeReceivedMessageLog(KeyValueMessage message){
		/* build ServerMessage String */
		log.info("RECEIVE \t<" 
				+ socket.getInetAddress().getHostAddress() + ":" 
				+ socket.getPort() + ">: '" 
				+ message.getStatus() + " " + message.getKey() + " " + message.getValue() + "'");
		
	}
	
	/**
	 * Method sends a KVMessage using this socket.
	 * @param msg the message that is to be sent.
	 * @throws IOException some I/O error regarding the output stream 
	 */
	public void sendMessage(KeyValueMessage msg) throws IOException {
		byte[] msgBytes = msg.toByteArray();
		out.write(msgBytes, 0, msgBytes.length);
		out.flush();
		log.info("SEND \t<" 
				+ socket.getInetAddress().getHostAddress() + ":" 
				+ socket.getPort() + ">: '" 
				+ msg.getStatus() +" " + msg.getKey() + " " + msg.getValue() + "'");
    }
	
	/**
	 * Extracts the key of a message
	 * @return Key of a message as a String
	 * @throws IOException
	 */
	private String extractKeyFromStream() throws IOException, CommandTooLongException{
		return extractNextComponentFromStream(KeyValueMessage.MAX_KEY_SIZE, false);
	}
	
	/**
	 * Extracts the value of a message
	 * @return Value of a message as a String
	 * @throws IOException
	 */
	private String extractValueFromStream() throws IOException, CommandTooLongException{
		return extractNextComponentFromStream(KeyValueMessage.MAX_VALUE_SIZE, true);
	}
	
	/**
	 * Extracts the status of a message
	 * @return Status of a message as a String
	 * @throws IOException
	 */
	private String extractStatusFromStream() throws IOException, CommandTooLongException{
		return extractNextComponentFromStream( 40, false );
	}
	
	/**
	 * Extracts the next component for the incoming Byte InputStream
	 * @param size The maximum size, which is allowed to be sent according to specification
	 * @return Returns the next component as a String
	 * @throws IOException
	 */
	private String extractNextComponentFromStream(int size, boolean retrieveValue) throws IOException, CommandTooLongException {
		int index = 0;
		byte[] bufferBytes = new byte[size + 1];

		/* read first char from stream */
		byte read = (byte) in.read();	
		
//		Read until first space character (separator between statustype and key)
//		try{ // Rethink whether we need this.
			while (read != KeyValueMessage.MESSAGE_DELIMITER && index <= size) {
				if (!retrieveValue && read == KeyValueMessage.COMPONENT_DELIMITER) { // In this case we retrieve no value. The component delimiter is a space. Values including space are allowed.
					break;
				}
				bufferBytes[index] = read;
				read = (byte) in.read();
				index++;
			}
//		} catch (ArrayIndexOutOfBoundsException e){
//			// Client disconnected
//			in.close();
//			out.close();
//			socket.close();
//			return null;
//		}
		// The given value does not correspond with the maximum size for this field
		if (index > size &&  ( read != KeyValueMessage.MESSAGE_DELIMITER || ( !retrieveValue && read != KeyValueMessage.COMPONENT_DELIMITER ))) {
			// Now the value we retrieved is too long (either Status, Key or Value)
			while(read != KeyValueMessage.MESSAGE_DELIMITER){
				read = (byte) in.read(); // Read till end of stream
			}
			throw new CommandTooLongException();
		}
		//End of message stream arrived; nothing to return.
		if (bufferBytes[0] == KeyValueMessage.MESSAGE_DELIMITER) {
			return null;
		}
		
		if (read == KeyValueMessage.MESSAGE_DELIMITER) {
			messageEndReached = true;
		}
		
		StringBuilder streamComponent = new StringBuilder();
		for (int i = 0; i < index; i++) {
			streamComponent.append((char) bufferBytes[i]);
		}
		
		return streamComponent.toString();
	}
}
